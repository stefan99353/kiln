use std::{sync::Arc, time::Duration};

use parking_lot::RwLock;

pub fn start_reload_interval(tera: Arc<RwLock<tera::Tera>>, interval: Option<Duration>) {
    if let Some(interval) = interval {
        let _handle = tokio::task::spawn(async move {
            tokio::time::sleep(interval).await;

            let mut write = tera.write();
            let tera_backup = write.clone();

            if let Err(err) = write.full_reload() {
                tracing::error!(err = %err, "Template reloading error");
                *write = tera_backup;
            }

            drop(write);
        });
    }
}
