use axum::http::StatusCode;
use serde::{Deserialize, Deserializer, Serializer};

pub fn serialize<S>(status: &StatusCode, ser: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    ser.serialize_u16(status.as_u16())
}

pub fn deserialize<'de, D>(de: D) -> Result<StatusCode, D::Error>
where
    D: Deserializer<'de>,
{
    let code: u16 = Deserialize::deserialize(de)?;
    StatusCode::from_u16(code).map_or_else(
        |_| {
            Err(serde::de::Error::invalid_value(
                serde::de::Unexpected::Unsigned(u64::from(code)),
                &"a value between 100 and 600",
            ))
        },
        Ok,
    )
}
