use std::str::FromStr;

use lettre::message::Mailbox;
use serde::{Deserialize, Deserializer, Serializer};

use crate::serde::mailbox;

pub fn serialize<S>(mailbox: &Option<Mailbox>, ser: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    match mailbox {
        None => ser.serialize_none(),
        Some(mb) => mailbox::serialize(mb, ser),
    }
}

pub fn deserialize<'de, D>(de: D) -> Result<Option<Mailbox>, D::Error>
where
    D: Deserializer<'de>,
{
    let mailbox: Option<String> = Deserialize::deserialize(de)?;

    mailbox
        .map(|mb| Mailbox::from_str(&mb).map_err(serde::de::Error::custom))
        .transpose()
}
