use std::str::FromStr;

use lettre::message::Mailbox;
use serde::{Deserialize, Deserializer, Serializer};

pub fn serialize<S>(mailbox: &Mailbox, ser: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    ser.serialize_str(&mailbox.to_string())
}

pub fn deserialize<'de, D>(de: D) -> Result<Mailbox, D::Error>
where
    D: Deserializer<'de>,
{
    let mailbox: String = Deserialize::deserialize(de)?;

    Mailbox::from_str(&mailbox).map_err(serde::de::Error::custom)
}
