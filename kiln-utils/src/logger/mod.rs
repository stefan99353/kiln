pub mod config;
pub mod error;

use std::sync::OnceLock;

pub use config::Config;
use tracing_appender::non_blocking::WorkerGuard;
use tracing_subscriber::{
    fmt, fmt::MakeWriter, layer::SubscriberExt, util::SubscriberInitExt, EnvFilter, Layer, Registry,
};

use crate::logger::config::{Format, LogLevel, Rotation};

static NONBLOCKING_WORK_GUARD_KEEP: OnceLock<WorkerGuard> = OnceLock::new();

pub fn init(config: &Config) -> error::Result<()> {
    let mut layers: Vec<Box<dyn Layer<Registry> + Send + Sync>> = Vec::new();

    if let Some(file_appender_config) = &config.file_appender {
        if file_appender_config.enable {
            let mut rolling_builder = tracing_appender::rolling::Builder::default();

            if let Some(max_log_files) = file_appender_config.max_log_files {
                rolling_builder = rolling_builder.max_log_files(max_log_files);
            }

            rolling_builder = match file_appender_config.rotation {
                Rotation::Minutely => {
                    rolling_builder.rotation(tracing_appender::rolling::Rotation::MINUTELY)
                }
                Rotation::Hourly => {
                    rolling_builder.rotation(tracing_appender::rolling::Rotation::HOURLY)
                }
                Rotation::Daily => {
                    rolling_builder.rotation(tracing_appender::rolling::Rotation::DAILY)
                }
                Rotation::Never => {
                    rolling_builder.rotation(tracing_appender::rolling::Rotation::NEVER)
                }
            };

            let file_appender = rolling_builder
                .filename_prefix(
                    file_appender_config
                        .filename_prefix
                        .as_ref()
                        .map_or_else(String::new, ToString::to_string),
                )
                .filename_suffix(
                    file_appender_config
                        .filename_suffix
                        .as_ref()
                        .map_or_else(String::new, ToString::to_string),
                )
                .build(&file_appender_config.dir)?;

            let (non_blocking_file_appender, work_guard) =
                tracing_appender::non_blocking(file_appender);
            NONBLOCKING_WORK_GUARD_KEEP
                .set(work_guard)
                .map_err(|_| error::Error::GuardCellLock)?;

            let file_appender_layer = init_layer(
                non_blocking_file_appender,
                file_appender_config.format,
                false,
            );

            layers.push(file_appender_layer);
        }
    }

    if config.enable {
        let stdout_layer = init_layer(std::io::stdout, config.format, true);
        layers.push(stdout_layer);
    }

    if !layers.is_empty() {
        let env_filter = init_env_filter(config.override_filter.as_ref(), config.level);
        tracing_subscriber::registry()
            .with(layers)
            .with(env_filter)
            .try_init()?;
    }

    if config.pretty_backtrace {
        std::env::set_var("RUST_BACKTRACE", "1");
    }

    Ok(())
}

fn init_env_filter(override_filter: Option<&String>, level: LogLevel) -> EnvFilter {
    EnvFilter::try_from_default_env()
        .or_else(|_| {
            override_filter
                .map_or_else(|| EnvFilter::try_new(level.to_string()), EnvFilter::try_new)
        })
        .expect("Logger initialization failed")
}

fn init_layer<W>(
    make_writer: W,
    format: Format,
    ansi: bool,
) -> Box<dyn Layer<Registry> + Send + Sync>
where
    W: for<'writer> MakeWriter<'writer> + Send + Sync + 'static,
{
    match format {
        Format::Compact => fmt::Layer::default()
            .with_ansi(ansi)
            .with_writer(make_writer)
            .compact()
            .boxed(),
        Format::Pretty => fmt::Layer::default()
            .with_ansi(ansi)
            .with_writer(make_writer)
            .pretty()
            .boxed(),
        Format::Json => fmt::Layer::default()
            .with_ansi(ansi)
            .with_writer(make_writer)
            .json()
            .boxed(),
    }
}
