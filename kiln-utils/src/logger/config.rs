use std::{fmt::Formatter, path::PathBuf};

use serde::{Deserialize, Serialize};

/// Configuration for the logger.
#[derive(Debug, Clone, Default, Deserialize, Serialize)]
#[serde(default)]
pub struct Config {
    /// Enables writing logs to stdout.
    ///
    /// Default: `false`
    pub enable: bool,

    /// Enables a nice display of backtraces.
    /// Has a slight impact on performance.
    /// Sets RUST_BACKTRACE=1
    ///
    /// Default: `false`
    pub pretty_backtrace: bool,

    /// Sets the logger level.
    ///
    /// Options: `trace` | `debug` | `info` | `warn` | `error`
    /// Default: `info`
    pub level: LogLevel,

    /// Sets the logger format.
    ///
    /// Options: `compact` | `pretty` | `json`
    /// Default: `compact`
    pub format: Format,

    /// Overrides the tracing filter.
    ///
    /// Set this to your own filter if you want to see traces from internal
    /// libraries. See more [here](https://docs.rs/tracing-subscriber/latest/tracing_subscriber/filter/struct.EnvFilter.html#directives)
    pub override_filter: Option<String>,

    /// Configures an optional file appender to log to a file.
    pub file_appender: Option<FileAppenderConfig>,
}

/// Configuration for the optional file appender.
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(default)]
pub struct FileAppenderConfig {
    /// Enables file appender.
    ///
    /// Default: `false`
    pub enable: bool,

    /// Sets the file appender log level.
    ///
    /// Options: `trace` | `debug` | `info` | `warn` | `error`
    /// Default: `info`
    pub level: LogLevel,

    /// Sets the file appender format.
    ///
    /// Options: `compact` | `pretty` | `json`
    /// Default: `compact`
    pub format: Format,

    /// Sets the file appender file rotation.
    ///
    /// Options: `minutely` | `hourly` | `daily` | `never`
    /// Default: `daily`
    pub rotation: Rotation,

    /// Sets the file appender dir.
    ///
    /// Default: `./logs`
    pub dir: PathBuf,

    /// Sets a log filename prefix.
    pub filename_prefix: Option<String>,

    /// Sets a log filename suffix.
    pub filename_suffix: Option<String>,

    /// Sets the file appender max files.
    pub max_log_files: Option<usize>,
}

impl Default for FileAppenderConfig {
    fn default() -> Self {
        Self {
            enable: false,
            level: Default::default(),
            format: Default::default(),
            rotation: Default::default(),
            dir: PathBuf::from("./logs"),
            filename_prefix: None,
            filename_suffix: None,
            max_log_files: None,
        }
    }
}

/// Basic log levels.
#[derive(Debug, Default, Copy, Clone, Deserialize, Serialize)]
pub enum LogLevel {
    /// No logs
    #[serde(rename = "off")]
    Off,

    /// Trace and higher
    #[serde(rename = "trace")]
    Trace,

    /// Debug and higher
    #[serde(rename = "debug")]
    Debug,

    /// Info and higher
    #[serde(rename = "info")]
    #[default]
    Info,

    /// Warn and higher
    #[serde(rename = "warn")]
    Warn,

    /// Only Error
    #[serde(rename = "error")]
    Error,
}

impl std::fmt::Display for LogLevel {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        serde_variant::to_variant_name(self)
            .expect("Only enums are supported")
            .fmt(f)
    }
}

/// Format of the logs.
#[derive(Debug, Default, Copy, Clone, Deserialize, Serialize)]
pub enum Format {
    /// Compact format
    #[serde(rename = "compact")]
    #[default]
    Compact,

    /// Pretty multi-line format
    #[serde(rename = "pretty")]
    Pretty,

    /// JSON formatted logs
    #[serde(rename = "json")]
    Json,
}

/// File rotation.
#[derive(Debug, Default, Copy, Clone, Deserialize, Serialize)]
pub enum Rotation {
    /// Rotate every minute
    #[serde(rename = "minutely")]
    Minutely,

    /// Rotate every hour
    #[serde(rename = "hourly")]
    Hourly,

    /// Rotate every day
    #[serde(rename = "daily")]
    #[default]
    Daily,

    /// No file rotation
    #[serde(rename = "never")]
    Never,
}
