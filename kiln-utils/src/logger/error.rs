pub(crate) type Result<T> = std::result::Result<T, Error>;

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error(transparent)]
    InitLogger(#[from] tracing_subscriber::util::TryInitError),

    #[error(transparent)]
    BuildRollingFileAppender(#[from] tracing_appender::rolling::InitError),

    #[error("Could not lock cell to keep nonblocking work guard")]
    GuardCellLock,
}
