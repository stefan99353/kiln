use sea_orm_migration::prelude::*;

use crate::m19000101_000001_user::User;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(ResetPasswordToken::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(ResetPasswordToken::Uuid)
                            .uuid()
                            .not_null()
                            .primary_key(),
                    )
                    .col(
                        ColumnDef::new(ResetPasswordToken::UserUuid)
                            .uuid()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(ResetPasswordToken::Token)
                            .string_len(128)
                            .not_null()
                            .unique_key(),
                    )
                    .col(
                        ColumnDef::new(ResetPasswordToken::Active)
                            .boolean()
                            .not_null()
                            .default(true),
                    )
                    .col(
                        ColumnDef::new(ResetPasswordToken::ExpiresAt)
                            .timestamp()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(ResetPasswordToken::CreatedAt)
                            .timestamp()
                            .not_null()
                            .default(Keyword::CurrentTimestamp),
                    )
                    .col(
                        ColumnDef::new(ResetPasswordToken::UpdatedAt)
                            .timestamp()
                            .not_null()
                            .default(Keyword::CurrentTimestamp),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .name("fk_reset_password_token_user")
                            .from(ResetPasswordToken::Table, ResetPasswordToken::UserUuid)
                            .to(User::Table, User::Uuid)
                            .on_update(ForeignKeyAction::Cascade)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .to_owned(),
            )
            .await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(
                Table::drop()
                    .table(ResetPasswordToken::Table)
                    .if_exists()
                    .to_owned(),
            )
            .await?;

        Ok(())
    }
}

#[derive(DeriveIden)]
pub enum ResetPasswordToken {
    Table,
    Uuid,
    UserUuid,
    Token,
    Active,
    ExpiresAt,
    CreatedAt,
    UpdatedAt,
}
