use sea_orm_migration::prelude::*;

use crate::m19000101_000001_user::User;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(UserAccessToken::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(UserAccessToken::Uuid)
                            .uuid()
                            .not_null()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(UserAccessToken::UserUuid).uuid().not_null())
                    .col(
                        ColumnDef::new(UserAccessToken::Name)
                            .string_len(256)
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(UserAccessToken::Token)
                            .string_len(128)
                            .not_null()
                            .unique_key(),
                    )
                    .col(
                        ColumnDef::new(UserAccessToken::Active)
                            .boolean()
                            .not_null()
                            .default(true),
                    )
                    .col(
                        ColumnDef::new(UserAccessToken::ExpiresAt)
                            .timestamp()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(UserAccessToken::CreatedAt)
                            .timestamp()
                            .not_null()
                            .default(Keyword::CurrentTimestamp),
                    )
                    .col(
                        ColumnDef::new(UserAccessToken::UpdatedAt)
                            .timestamp()
                            .not_null()
                            .default(Keyword::CurrentTimestamp),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .name("fk_user_access_token_user")
                            .from(UserAccessToken::Table, UserAccessToken::UserUuid)
                            .to(User::Table, User::Uuid)
                            .on_update(ForeignKeyAction::Cascade)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .to_owned(),
            )
            .await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(
                Table::drop()
                    .table(UserAccessToken::Table)
                    .if_exists()
                    .to_owned(),
            )
            .await?;

        Ok(())
    }
}

#[derive(DeriveIden)]
pub enum UserAccessToken {
    Table,
    Uuid,
    UserUuid,
    Name,
    Token,
    Active,
    ExpiresAt,
    CreatedAt,
    UpdatedAt,
}
