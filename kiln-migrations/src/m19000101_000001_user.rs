use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(User::Table)
                    .if_not_exists()
                    .col(ColumnDef::new(User::Uuid).uuid().not_null().primary_key())
                    .col(
                        ColumnDef::new(User::Email)
                            .string_len(320)
                            .not_null()
                            .unique_key(),
                    )
                    .col(
                        ColumnDef::new(User::Handle)
                            .string_len(32)
                            .not_null()
                            .unique_key(),
                    )
                    .col(ColumnDef::new(User::FirstName).string_len(128).null())
                    .col(ColumnDef::new(User::LastName).string_len(128).null())
                    .col(ColumnDef::new(User::PasswordHash).string_len(256).null())
                    .col(
                        ColumnDef::new(User::Verified)
                            .boolean()
                            .not_null()
                            .default(false),
                    )
                    .col(
                        ColumnDef::new(User::Active)
                            .boolean()
                            .not_null()
                            .default(false),
                    )
                    .col(ColumnDef::new(User::VerifiedAt).timestamp().null())
                    .col(
                        ColumnDef::new(User::CreatedAt)
                            .timestamp()
                            .not_null()
                            .default(Keyword::CurrentTimestamp),
                    )
                    .col(
                        ColumnDef::new(User::UpdatedAt)
                            .timestamp()
                            .not_null()
                            .default(Keyword::CurrentTimestamp),
                    )
                    .to_owned(),
            )
            .await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(User::Table).if_exists().to_owned())
            .await?;

        Ok(())
    }
}

#[derive(DeriveIden)]
pub enum User {
    Table,
    Uuid,
    Email,
    Handle,
    FirstName,
    LastName,
    PasswordHash,
    Verified,
    Active,
    VerifiedAt,
    CreatedAt,
    UpdatedAt,
}
