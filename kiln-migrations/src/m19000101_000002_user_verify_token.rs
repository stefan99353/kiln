use sea_orm_migration::prelude::*;

use crate::m19000101_000001_user::User;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(UserVerifyToken::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(UserVerifyToken::Uuid)
                            .uuid()
                            .not_null()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(UserVerifyToken::UserUuid).uuid().not_null())
                    .col(
                        ColumnDef::new(UserVerifyToken::Token)
                            .string_len(128)
                            .not_null()
                            .unique_key(),
                    )
                    .col(
                        ColumnDef::new(UserVerifyToken::Active)
                            .boolean()
                            .not_null()
                            .default(true),
                    )
                    .col(
                        ColumnDef::new(UserVerifyToken::CreatedAt)
                            .timestamp()
                            .not_null()
                            .default(Keyword::CurrentTimestamp),
                    )
                    .col(
                        ColumnDef::new(UserVerifyToken::UpdatedAt)
                            .timestamp()
                            .not_null()
                            .default(Keyword::CurrentTimestamp),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .name("fk_user_verify_token_user")
                            .from(UserVerifyToken::Table, UserVerifyToken::UserUuid)
                            .to(User::Table, User::Uuid)
                            .on_update(ForeignKeyAction::Cascade)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .to_owned(),
            )
            .await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(
                Table::drop()
                    .table(UserVerifyToken::Table)
                    .if_exists()
                    .to_owned(),
            )
            .await?;

        Ok(())
    }
}

#[derive(DeriveIden)]
pub enum UserVerifyToken {
    Table,
    Uuid,
    UserUuid,
    Token,
    Active,
    CreatedAt,
    UpdatedAt,
}
