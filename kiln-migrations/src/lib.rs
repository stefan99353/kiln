mod m19000101_000001_user;
mod m19000101_000002_user_verify_token;
mod m19000101_000003_reset_password_token;
mod m19000101_000004_user_access_token;
mod m19000101_000005_queued_task;

pub use sea_orm_migration::MigratorTrait;
use sea_orm_migration::{prelude::*, sea_orm::DbConn};

pub struct Migrator;

#[async_trait::async_trait]
impl MigratorTrait for Migrator {
    fn migrations() -> Vec<Box<dyn MigrationTrait>> {
        vec![
            Box::new(m19000101_000001_user::Migration),
            Box::new(m19000101_000002_user_verify_token::Migration),
            Box::new(m19000101_000003_reset_password_token::Migration),
            Box::new(m19000101_000004_user_access_token::Migration),
            Box::new(m19000101_000005_queued_task::Migration),
        ]
    }

    fn migration_table_name() -> DynIden {
        Alias::new("kiln_migration").into_iden()
    }
}

pub async fn converge(migrate: bool, recreate: bool, db: &DbConn) -> Result<(), DbErr> {
    if recreate {
        Migrator::fresh(db).await?;
    }

    if migrate || recreate {
        Migrator::up(db, None).await?;
    }

    Ok(())
}
