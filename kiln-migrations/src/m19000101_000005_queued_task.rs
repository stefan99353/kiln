use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(QueuedTask::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(QueuedTask::Uuid)
                            .uuid()
                            .not_null()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(QueuedTask::Name).string_len(256).not_null())
                    .col(
                        ColumnDef::new(QueuedTask::Priority)
                            .small_integer()
                            .not_null()
                            .default(0),
                    )
                    .col(
                        ColumnDef::new(QueuedTask::ScheduledFor)
                            .timestamp()
                            .not_null()
                            .default(Keyword::CurrentTimestamp),
                    )
                    .col(
                        ColumnDef::new(QueuedTask::Status)
                            .string_len(64)
                            .not_null()
                            .default("new"),
                    )
                    .col(ColumnDef::new(QueuedTask::Args).json_binary().null())
                    .col(ColumnDef::new(QueuedTask::QueuedAt).timestamp().null())
                    .col(ColumnDef::new(QueuedTask::StartedAt).timestamp().null())
                    .col(ColumnDef::new(QueuedTask::CompletedAt).timestamp().null())
                    .col(ColumnDef::new(QueuedTask::FailedAt).timestamp().null())
                    .col(ColumnDef::new(QueuedTask::CancelledAt).timestamp().null())
                    .col(
                        ColumnDef::new(QueuedTask::CreatedAt)
                            .timestamp()
                            .not_null()
                            .default(Keyword::CurrentTimestamp),
                    )
                    .col(
                        ColumnDef::new(QueuedTask::UpdatedAt)
                            .timestamp()
                            .not_null()
                            .default(Keyword::CurrentTimestamp),
                    )
                    .to_owned(),
            )
            .await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(
                Table::drop()
                    .table(QueuedTask::Table)
                    .if_exists()
                    .to_owned(),
            )
            .await?;

        Ok(())
    }
}

#[derive(DeriveIden)]
pub enum QueuedTask {
    Table,
    Uuid,
    Name,
    Priority,
    ScheduledFor,
    Status,
    Args,
    QueuedAt,
    StartedAt,
    CompletedAt,
    FailedAt,
    CancelledAt,
    CreatedAt,
    UpdatedAt,
}
