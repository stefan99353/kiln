use argon2::{password_hash::SaltString, Argon2, PasswordHash, PasswordHasher, PasswordVerifier};
use passwords::analyzer::analyze;
use rand::rngs::OsRng;
use validator::ValidationError;

pub fn hash_password(password: &[u8]) -> Result<String, password_hash::Error> {
    let argon2 = Argon2::default();
    let salt = SaltString::generate(&mut OsRng);

    let hash = argon2.hash_password(password, &salt)?.to_string();

    Ok(hash)
}

pub fn validate_password(password: &str) -> Result<(), ValidationError> {
    let analyzed = analyze(password);

    if analyzed.spaces_count() > 0 {
        return Err(ValidationError::new("invalid_password_characters"));
    }

    if analyzed.numbers_count() == 0
        || analyzed.lowercase_letters_count() == 0
        || analyzed.uppercase_letters_count() == 0
        || (analyzed.symbols_count() == 0 && analyzed.other_characters_count() == 0)
    {
        return Err(ValidationError::new("too_weak_password"));
    }

    Ok(())
}

pub fn verify_password(password: &str, hash: &str) -> Result<bool, password_hash::Error> {
    let parsed_hash = PasswordHash::new(hash)?;

    let argon = Argon2::default();

    let verified = argon.verify_password(password.as_bytes(), &parsed_hash);

    match verified {
        Ok(_) => Ok(true),
        Err(err) => {
            if let password_hash::Error::Password = err {
                return Ok(false);
            }

            Err(err)
        }
    }
}
