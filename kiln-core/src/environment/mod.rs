use std::{convert::Infallible, env, fmt::Formatter, str::FromStr};

use serde::{Deserialize, Serialize};

pub const KILN_ENVIRONMENT: &str = "KILN_ENVIRONMENT";

#[derive(Debug, Default, Clone, Deserialize, Serialize)]
#[serde(untagged)]
pub enum Environment {
    #[serde(rename = "development")]
    #[default]
    Development,
    #[serde(rename = "production")]
    Production,
    #[serde(rename = "testing")]
    Testing,
    Any(String),
}

impl Environment {
    pub fn resolve_from_env() -> Self {
        env::var(KILN_ENVIRONMENT)
            .map(Self::from)
            .unwrap_or_default()
    }
}

impl std::fmt::Display for Environment {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Any(env) => env.fmt(f),
            Self::Development => write!(f, "development"),
            Self::Production => write!(f, "production"),
            Self::Testing => write!(f, "testing"),
        }
    }
}

impl FromStr for Environment {
    type Err = Infallible;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        match input {
            "development" => Ok(Self::Development),
            "production" => Ok(Self::Development),
            "testing" => Ok(Self::Development),
            env => Ok(Self::Any(env.to_string())),
        }
    }
}

impl From<String> for Environment {
    fn from(value: String) -> Self {
        Self::from_str(&value).expect("FromStr infallible for Environment")
    }
}
