use chrono::Utc;
use sea_orm::{entity::prelude::*, ActiveValue, IntoActiveModel, TransactionTrait};
use serde::{Deserialize, Serialize};
use validator::Validate;

pub use super::_entities::user::{self, ActiveModel, Column, Entity, Model, Relation};
use crate::{
    auth,
    error::{KilnError, KilnResult},
    models::{error::ModelError, reset_password_token, user_verify_token},
};

#[derive(Debug, Clone, Deserialize, Serialize, Validate)]
pub struct RegisterData {
    #[validate(email)]
    pub email: String,
    #[validate(length(min = 1, max = 32))]
    pub handle: String,
    #[validate(length(min = 1, max = 128))]
    pub first_name: Option<String>,
    #[validate(length(min = 1, max = 128))]
    pub last_name: Option<String>,
    #[validate(
        length(min = 8, max = 64),
        custom(function = "auth::utils::validate_password")
    )]
    pub password: String,
}

#[async_trait::async_trait]
impl ActiveModelBehavior for ActiveModel {
    async fn before_save<C>(mut self, _db: &C, insert: bool) -> Result<Self, DbErr>
    where
        C: ConnectionTrait,
    {
        let now = Utc::now().naive_utc();

        if insert {
            self.uuid = ActiveValue::Set(Uuid::now_v7());
            self.created_at = ActiveValue::Set(now);
        }

        self.updated_at = ActiveValue::Set(now);

        Ok(self)
    }
}

impl Model {
    pub async fn find_by_email<C: ConnectionTrait>(
        email: &str,
        db: &C,
    ) -> KilnResult<Option<Self>> {
        let model = Entity::find()
            .filter(Column::Email.eq(email))
            .one(db)
            .await?;

        Ok(model)
    }

    pub async fn find_by_handle<C: ConnectionTrait>(
        handle: &str,
        db: &C,
    ) -> KilnResult<Option<Self>> {
        let model = Entity::find()
            .filter(Column::Handle.eq(handle))
            .one(db)
            .await?;

        Ok(model)
    }

    pub async fn create_with_password(
        data: RegisterData,
        db: &DbConn,
    ) -> KilnResult<(Self, user_verify_token::Model)> {
        let password_hash = tokio::task::spawn_blocking(move || {
            auth::utils::hash_password(data.password.as_bytes())
        })
        .await
        .expect("Password hashing panicked")
        .map_err(KilnError::any)?;

        let (user, verify_token) = db
            .transaction::<_, (Self, user_verify_token::Model), KilnError>(|txn| {
                Box::pin(async move {
                    // Check if user exists (email or handle)
                    let handle_exists = Self::find_by_handle(&data.handle, txn).await?.is_some();

                    let email_exists = Self::find_by_email(&data.email, txn).await?.is_some();

                    if handle_exists || email_exists {
                        return Err(ModelError::EntityExists.into());
                    }

                    let mut active_model = ActiveModel {
                        email: ActiveValue::Set(data.email),
                        handle: ActiveValue::Set(data.handle),
                        password_hash: ActiveValue::Set(Some(password_hash)),
                        active: ActiveValue::Set(true),
                        ..std::default::Default::default()
                    };

                    if let Some(first_name) = data.first_name {
                        active_model.first_name = ActiveValue::Set(Some(first_name));
                    }

                    if let Some(last_name) = data.last_name {
                        active_model.last_name = ActiveValue::Set(Some(last_name));
                    }

                    let user = active_model.insert(txn).await?;
                    let user_verify_token =
                        user_verify_token::Model::create_token(user.uuid, txn).await?;

                    Ok((user, user_verify_token))
                })
            })
            .await?;

        Ok((user, verify_token))
    }

    pub fn name(&self) -> Option<String> {
        match (&self.first_name, &self.last_name) {
            (None, None) => None,
            (Some(first), None) => Some(first.to_string()),
            (None, Some(last)) => Some(last.to_string()),
            (Some(first), Some(last)) => Some(format!("{first} {last}")),
        }
    }
}

impl ActiveModel {
    pub async fn verify<C: ConnectionTrait>(mut self, db: &C) -> KilnResult<Model> {
        self.verified = ActiveValue::Set(true);
        self.verified_at = ActiveValue::Set(Some(Utc::now().naive_utc()));
        Ok(self.update(db).await?)
    }

    pub async fn reset_password<T: TransactionTrait>(
        mut self,
        password: String,
        reset_token: reset_password_token::Model,
        db: &T,
    ) -> KilnResult<Model> {
        let password_hash =
            tokio::task::spawn_blocking(move || auth::utils::hash_password(password.as_bytes()))
                .await
                .expect("Password hashing panicked")
                .map_err(KilnError::any)?;

        let user = db
            .transaction::<_, Model, KilnError>(|txn| {
                Box::pin(async move {
                    self.password_hash = ActiveValue::Set(Some(password_hash));
                    let user = self.update(txn).await?;

                    reset_token.into_active_model().deactivate(txn).await?;

                    Ok(user)
                })
            })
            .await?;

        Ok(user)
    }
}
