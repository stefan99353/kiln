use chrono::{NaiveDateTime, Utc};
use sea_orm::{
    entity::prelude::*, ActiveValue, QueryOrder, QuerySelect, TransactionError, TransactionTrait,
};

pub use super::_entities::queued_task::{
    self, ActiveModel, Column, Entity, Model, Relation, TaskPriority, TaskStatus,
};
use crate::error::KilnResult;

#[async_trait::async_trait]
impl ActiveModelBehavior for ActiveModel {
    async fn before_save<C>(mut self, _db: &C, insert: bool) -> Result<Self, DbErr>
    where
        C: ConnectionTrait,
    {
        let now = Utc::now().naive_utc();

        if insert {
            self.uuid = ActiveValue::Set(Uuid::now_v7());
            self.created_at = ActiveValue::Set(now);
        }

        self.updated_at = ActiveValue::Set(now);

        Ok(self)
    }
}

impl Model {
    pub async fn enqueue_task<C: ConnectionTrait>(
        name: String,
        priority: TaskPriority,
        args: Option<serde_json::Value>,
        schedule_for: NaiveDateTime,
        db: &C,
    ) -> KilnResult<Self> {
        let queued_task = ActiveModel {
            name: ActiveValue::Set(name),
            priority: ActiveValue::Set(priority),
            scheduled_for: ActiveValue::Set(schedule_for),
            status: ActiveValue::Set(TaskStatus::New),
            args: ActiveValue::Set(args),
            ..Default::default()
        };

        let model = queued_task.insert(db).await?;
        Ok(model)
    }

    pub async fn dequeue_tasks(n: u64, db: &DbConn) -> KilnResult<Vec<Self>> {
        let queued_tasks = db
            .transaction::<_, Vec<Self>, DbErr>(|txn| {
                Box::pin(async move {
                    let tasks = Entity::find()
                        .filter(Column::Status.eq(TaskStatus::New))
                        .filter(Column::ScheduledFor.lte(Utc::now().naive_utc()))
                        .order_by_desc(Column::Priority)
                        .order_by_asc(Column::ScheduledFor)
                        .limit(n)
                        .all(txn)
                        .await?;

                    let task_ids = tasks.iter().map(|t| t.uuid);

                    Entity::update_many()
                        .col_expr(Column::Status, Expr::value(TaskStatus::Queued))
                        .col_expr(Column::UpdatedAt, Expr::value(Utc::now().naive_utc()))
                        .filter(Column::Uuid.is_in(task_ids))
                        .exec(txn)
                        .await?;

                    Ok(tasks)
                })
            })
            .await
            .map_err(|err| match err {
                TransactionError::Connection(err) => err,
                TransactionError::Transaction(err) => err,
            })?;

        Ok(queued_tasks)
    }
}

impl ActiveModel {
    pub async fn start<C: ConnectionTrait>(mut self, db: &C) -> KilnResult<Model> {
        self.status = ActiveValue::Set(TaskStatus::Running);
        self.started_at = ActiveValue::Set(Some(Utc::now().naive_utc()));

        let model = self.update(db).await?;
        Ok(model)
    }

    pub async fn complete<C: ConnectionTrait>(mut self, db: &C) -> KilnResult<Model> {
        self.status = ActiveValue::Set(TaskStatus::Completed);
        self.completed_at = ActiveValue::Set(Some(Utc::now().naive_utc()));

        let model = self.update(db).await?;
        Ok(model)
    }

    pub async fn fail<C: ConnectionTrait>(mut self, db: &C) -> KilnResult<Model> {
        self.status = ActiveValue::Set(TaskStatus::Failed);
        self.failed_at = ActiveValue::Set(Some(Utc::now().naive_utc()));

        let model = self.update(db).await?;
        Ok(model)
    }

    pub async fn cancel<C: ConnectionTrait>(mut self, db: &C) -> KilnResult<Model> {
        self.status = ActiveValue::Set(TaskStatus::Cancelled);
        self.cancelled_at = ActiveValue::Set(Some(Utc::now().naive_utc()));

        let model = self.update(db).await?;
        Ok(model)
    }
}
