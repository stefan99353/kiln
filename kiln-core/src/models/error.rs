#[derive(Debug, thiserror::Error)]
pub enum ModelError {
    #[error("Entity already exists")]
    EntityExists,

    #[error("Entity was not found")]
    EntityNotFound(String),
}
