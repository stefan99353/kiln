use chrono::Utc;
use sea_orm::{entity::prelude::*, ActiveValue};
use serde::{Deserialize, Serialize};

pub use super::_entities::user_access_token::{self, ActiveModel, Column, Entity, Model, Relation};

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct RegisterData {
    test: (),
}

#[async_trait::async_trait]
impl ActiveModelBehavior for ActiveModel {
    async fn before_save<C>(mut self, _db: &C, insert: bool) -> Result<Self, DbErr>
    where
        C: ConnectionTrait,
    {
        let now = Utc::now().naive_utc();

        if insert {
            self.uuid = ActiveValue::Set(Uuid::now_v7());
            self.created_at = ActiveValue::Set(now);
        }

        self.updated_at = ActiveValue::Set(now);

        Ok(self)
    }
}

impl Model {}

impl ActiveModel {}
