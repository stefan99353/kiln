use chrono::Utc;
use rand::{
    distributions::{Alphanumeric, DistString},
    rngs::OsRng,
};
use sea_orm::{entity::prelude::*, ActiveValue, TransactionTrait};
use serde::{Deserialize, Serialize};

pub use super::_entities::user_verify_token::{self, ActiveModel, Column, Entity, Model, Relation};
use crate::error::KilnResult;

#[derive(Debug, Clone, PartialEq, Eq, Deserialize, Serialize)]
pub struct RegisterData {
    test: (),
}

#[async_trait::async_trait]
impl ActiveModelBehavior for ActiveModel {
    async fn before_save<C>(mut self, _db: &C, insert: bool) -> Result<Self, DbErr>
    where
        C: ConnectionTrait,
    {
        let now = Utc::now().naive_utc();

        if insert {
            self.uuid = ActiveValue::Set(Uuid::now_v7());
            self.created_at = ActiveValue::Set(now);
        }

        self.updated_at = ActiveValue::Set(now);

        Ok(self)
    }
}

impl Model {
    pub async fn find_by_token<C: ConnectionTrait>(
        token: &str,
        db: &C,
    ) -> KilnResult<Option<Self>> {
        let model = Entity::find()
            .filter(Column::Token.eq(token))
            .one(db)
            .await?;

        Ok(model)
    }

    pub async fn create_token<T: TransactionTrait>(user_uuid: Uuid, db: &T) -> KilnResult<Self> {
        let token = Alphanumeric.sample_string(&mut OsRng, 128);

        let model = db
            .transaction::<_, Self, DbErr>(|txn| {
                Box::pin(async move {
                    Entity::update_many()
                        .col_expr(Column::Active, Expr::value(false))
                        .col_expr(Column::UpdatedAt, Expr::value(Utc::now().naive_utc()))
                        .filter(Column::UserUuid.eq(user_uuid))
                        .exec(txn)
                        .await?;

                    let user_verify_token = ActiveModel {
                        user_uuid: ActiveValue::Set(user_uuid),
                        token: ActiveValue::Set(token),
                        ..Default::default()
                    };

                    let model = user_verify_token.insert(txn).await?;
                    Ok(model)
                })
            })
            .await?;

        Ok(model)
    }
}

impl ActiveModel {
    pub async fn deactivate<C: ConnectionTrait>(mut self, db: &C) -> KilnResult<Model> {
        self.active = ActiveValue::Set(false);
        Ok(self.update(db).await?)
    }
}
