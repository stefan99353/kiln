use std::fmt::Formatter;

use sea_orm::entity::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq, Eq, DeriveEntityModel, Deserialize, Serialize)]
#[sea_orm(table_name = "queued_task")]
pub struct Model {
    #[sea_orm(primary_key, auto_increment = false)]
    pub uuid: Uuid,
    pub name: String,
    pub priority: TaskPriority,
    pub scheduled_for: DateTime,
    pub status: TaskStatus,
    pub args: Option<Json>,
    pub queued_at: Option<DateTime>,
    pub started_at: Option<DateTime>,
    pub completed_at: Option<DateTime>,
    pub failed_at: Option<DateTime>,
    pub cancelled_at: Option<DateTime>,
    pub created_at: DateTime,
    pub updated_at: DateTime,
}

#[derive(
    Debug, Copy, Clone, Default, PartialEq, Eq, EnumIter, DeriveActiveEnum, Deserialize, Serialize,
)]
#[sea_orm(rs_type = "i16", db_type = "SmallInteger")]
pub enum TaskPriority {
    VeryLow = 1,
    Low = 2,
    #[default]
    Normal = 3,
    High = 4,
    VeryHigh = 5,
}

impl std::fmt::Display for TaskPriority {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        serde_variant::to_variant_name(self)
            .expect("Only enums are supported")
            .fmt(f)
    }
}

#[derive(
    Debug, Copy, Clone, Default, PartialEq, Eq, EnumIter, DeriveActiveEnum, Deserialize, Serialize,
)]
#[sea_orm(
    rs_type = "String",
    db_type = "String(StringLen::N(64))",
    rename_all = "camelCase"
)]
pub enum TaskStatus {
    #[default]
    New,
    Queued,
    Running,
    Completed,
    Failed,
    Cancelled,
}

impl std::fmt::Display for TaskStatus {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        serde_variant::to_variant_name(self)
            .expect("Only enums are supported")
            .fmt(f)
    }
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {}
