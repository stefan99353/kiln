use sea_orm::entity::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq, Eq, DeriveEntityModel, Deserialize, Serialize)]
#[sea_orm(table_name = "user")]
pub struct Model {
    #[sea_orm(primary_key, auto_increment = false)]
    pub uuid: Uuid,
    #[sea_orm(unique)]
    pub email: String,
    #[sea_orm(unique)]
    pub handle: String,
    pub first_name: Option<String>,
    pub last_name: Option<String>,
    pub password_hash: Option<String>,
    pub verified: bool,
    pub active: bool,
    pub verified_at: Option<DateTime>,
    pub created_at: DateTime,
    pub updated_at: DateTime,
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(has_many = "super::user_verify_token::Entity")]
    UserVerifyToken,
    #[sea_orm(has_many = "super::reset_password_token::Entity")]
    ResetPasswordToken,
    #[sea_orm(has_many = "super::user_access_token::Entity")]
    UserAccessToken,
}

impl Related<super::user_verify_token::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::UserVerifyToken.def()
    }
}

impl Related<super::reset_password_token::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::ResetPasswordToken.def()
    }
}

impl Related<super::user_access_token::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::UserAccessToken.def()
    }
}
