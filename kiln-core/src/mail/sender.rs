use std::sync::Arc;

use lettre::{
    message::Mailbox,
    transport::smtp::{authentication::Credentials, extension::ClientId, PoolConfig},
    AsyncFileTransport, AsyncSmtpTransport, AsyncTransport, Message, Tokio1Executor,
};

use crate::{
    error::KilnResult,
    mail::{
        config::{
            default_from, FileMailerConfig, MailerConfig, MailerTransportConfig, SmtpMailerConfig,
            SmtpTlsMode,
        },
        email::Email,
        error::MailerError,
    },
};

#[derive(Debug, Clone)]
pub struct EmailSender {
    default_from: Mailbox,
    default_reply_to: Option<Mailbox>,
    transport: MailTransport,
}

#[derive(Debug, Clone)]
pub enum MailTransport {
    None,
    Null,
    File(Arc<AsyncFileTransport<Tokio1Executor>>),
    Smtp(AsyncSmtpTransport<Tokio1Executor>),
}

impl EmailSender {
    pub async fn send_mail(&self, mut email: Email) -> KilnResult<()> {
        email.from = email.from.or(Some(self.default_from.clone().into()));

        if let (Some(reply_to), true) = (&self.default_reply_to, email.reply_to.is_empty()) {
            email.reply_to = vec![reply_to.clone().into()]
        }

        let message = Message::try_from(email).map_err(MailerError::Lettre)?;

        self.transport.send(message).await?;

        Ok(())
    }

    pub fn connect(config: Option<&MailerConfig>) -> KilnResult<Self> {
        let transport = match config {
            None => MailTransport::None,
            Some(config) => match &config.transport {
                MailerTransportConfig::Null => MailTransport::Null,
                MailerTransportConfig::File(file_config) => {
                    MailTransport::File(build_file_transport(file_config))
                }
                MailerTransportConfig::Smtp(smtp_config) => MailTransport::Smtp(
                    build_smtp_transport(smtp_config).map_err(MailerError::LettreSmtp)?,
                ),
            },
        };

        let sender = Self {
            default_from: config
                .map(|c| c.default_from.clone())
                .unwrap_or_else(default_from),
            default_reply_to: None,
            transport,
        };

        Ok(sender)
    }
}

impl MailTransport {
    pub async fn send(&self, message: Message) -> KilnResult<()> {
        match self {
            MailTransport::None => {
                let error = MailerError::MailerNotConfigured;
                tracing::error!(email = ?message, err = %error, "Mailer error");
                return Err(error.into());
            }
            MailTransport::Null => {
                tracing::trace!(email = ?message, "Sent mail into the void");
            }
            MailTransport::File(transport) => {
                transport
                    .send(message)
                    .await
                    .map_err(MailerError::LettreFile)?;
            }
            MailTransport::Smtp(transport) => {
                transport
                    .send(message)
                    .await
                    .map_err(MailerError::LettreSmtp)?;
            }
        }

        Ok(())
    }
}

pub fn build_smtp_transport(
    config: &SmtpMailerConfig,
) -> Result<AsyncSmtpTransport<Tokio1Executor>, lettre::transport::smtp::Error> {
    let pool_config = PoolConfig::new()
        .min_idle(config.pool.min_connections)
        .max_size(config.pool.max_connections)
        .idle_timeout(config.pool.idle_timeout);

    let mut builder = match config.tls_mode {
        SmtpTlsMode::None => {
            AsyncSmtpTransport::<Tokio1Executor>::builder_dangerous(&config.relay).port(25)
        }
        SmtpTlsMode::StartTls => {
            AsyncSmtpTransport::<Tokio1Executor>::starttls_relay(&config.relay)?
        }
        SmtpTlsMode::Full => AsyncSmtpTransport::<Tokio1Executor>::relay(&config.relay)?,
    };

    if let Some(port) = config.port {
        builder = builder.port(port);
    }

    if let Some(creds) = &config.credentials {
        builder = builder.credentials(Credentials::new(creds.user.clone(), creds.password.clone()));
    }

    if let Some(hello) = &config.hello_name {
        builder = builder.hello_name(ClientId::Domain(hello.clone()));
    }

    if let Some(timeout) = config.timeout {
        builder = builder.timeout(Some(timeout));
    }

    let transport = builder.pool_config(pool_config).build();

    Ok(transport)
}

pub fn build_file_transport(config: &FileMailerConfig) -> Arc<AsyncFileTransport<Tokio1Executor>> {
    Arc::new(AsyncFileTransport::new(&config.path))
}
