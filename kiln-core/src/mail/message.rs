use std::future::Future;

use serde::{Deserialize, Serialize};

use crate::{
    context::KilnContext,
    error::{KilnError, KilnResult},
    mail::{
        email::{BodyContent, Content, Mailbox},
        error::MailerError,
    },
    models::queued_task,
    task::{
        tasks::mailer::{MailerTask, MailerTaskArgs},
        Task,
    },
};

pub trait EmailMessage {
    fn subject(&self) -> Content;

    fn body(&self) -> BodyContent;

    fn send(
        self,
        args: EmailArgs,
        ctx: &KilnContext,
    ) -> impl Future<Output = KilnResult<queued_task::Model>>
    where
        Self: Serialize + Sized,
    {
        async move {
            if args.to.is_empty() {
                return Err(KilnError::msg("Missing recipient for E-Mail"));
            }

            let args = MailerTaskArgs {
                args,
                subject: self.subject(),
                body: self.body(),
                data: serde_json::to_value(&self).map_err(MailerError::SerializeRenderCtx)?,
            };

            let task = MailerTask::schedule(Some(args), ctx).await?;

            Ok(task)
        }
    }
}

#[derive(Debug, Clone, Default, Deserialize, Serialize)]
pub struct EmailArgs {
    pub from: Option<Mailbox>,
    pub to: Vec<Mailbox>,
    pub cc: Vec<Mailbox>,
    pub bcc: Vec<Mailbox>,
    pub sender: Option<Mailbox>,
    pub reply_to: Vec<Mailbox>,
}
