use serde::{Deserialize, Serialize};

use crate::mail::{
    email::{BodyContent, Content},
    message::EmailMessage,
};

#[derive(Debug, Deserialize, Serialize)]
pub struct VerifyUser {
    pub handle: String,
    pub first_name: Option<String>,
    pub last_name: Option<String>,
    pub domain: String,
    pub verify_token: String,
}

impl EmailMessage for VerifyUser {
    fn subject(&self) -> Content {
        Content::Template {
            template_name: "auth/verify_user/subject.tera".to_string(),
        }
    }

    fn body(&self) -> BodyContent {
        BodyContent::Multipart {
            plain: Content::Template {
                template_name: "auth/verify_user/plain.tera".to_string(),
            },
            html: Content::Template {
                template_name: "auth/verify_user/html.tera".to_string(),
            },
        }
    }
}
