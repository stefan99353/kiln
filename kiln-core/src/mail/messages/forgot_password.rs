use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};

use crate::mail::{
    email::{BodyContent, Content},
    message::EmailMessage,
};

#[derive(Debug, Deserialize, Serialize)]
pub struct ForgotPassword {
    pub handle: String,
    pub domain: String,
    pub reset_token: String,
    pub expires_at: NaiveDateTime,
}

impl EmailMessage for ForgotPassword {
    fn subject(&self) -> Content {
        Content::Template {
            template_name: "auth/forgot_password/subject.tera".to_string(),
        }
    }

    fn body(&self) -> BodyContent {
        BodyContent::Multipart {
            plain: Content::Template {
                template_name: "auth/forgot_password/plain.tera".to_string(),
            },
            html: Content::Template {
                template_name: "auth/forgot_password/html.tera".to_string(),
            },
        }
    }
}
