#[derive(Debug, thiserror::Error)]
pub enum MailerError {
    #[error("Mailer component was not configured")]
    MailerNotConfigured,

    #[error(transparent)]
    Lettre(#[from] lettre::error::Error),

    #[error(transparent)]
    LettreAddress(#[from] lettre::address::AddressError),

    #[error(transparent)]
    LettreSmtp(#[from] lettre::transport::smtp::Error),

    #[error(transparent)]
    LettreFile(#[from] lettre::transport::file::Error),

    #[error(transparent)]
    SerializeRenderCtx(serde_json::Error),
}
