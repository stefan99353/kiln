use std::str::FromStr;

use serde::{Deserialize, Serialize};

use crate::{context::KilnContext, mail::error::MailerError};

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Mailbox(#[serde(with = "kiln_utils::serde::mailbox")] lettre::message::Mailbox);

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Email {
    pub from: Option<Mailbox>,
    pub to: Vec<Mailbox>,
    pub cc: Vec<Mailbox>,
    pub bcc: Vec<Mailbox>,
    pub sender: Option<Mailbox>,
    pub reply_to: Vec<Mailbox>,
    pub subject: Subject,
    pub body: EmailBody,
}

pub type Subject = String;

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(tag = "kind")]
pub enum EmailBody {
    #[serde(rename = "plain")]
    Plain { content: String },
    #[serde(rename = "html")]
    Html { content: String },
    #[serde(rename = "multipart")]
    Multipart { plain: String, html: String },
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(tag = "kind")]
pub enum Content {
    #[serde(rename = "text")]
    Text { content: String },
    #[serde(rename = "template")]
    Template { template_name: String },
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(tag = "kind")]
pub enum BodyContent {
    #[serde(rename = "plain")]
    Plain(Content),
    #[serde(rename = "html")]
    Html(Content),
    #[serde(rename = "multipart")]
    Multipart { plain: Content, html: Content },
}

pub async fn render_mail(
    subject: Content,
    body: BodyContent,
    data: tera::Context,
    ctx: &KilnContext,
) -> Result<(Subject, EmailBody), tera::Error> {
    let ctx = ctx.clone();

    tokio::task::spawn_blocking(move || {
        let tera = ctx.template_engine.read();
        let subject = subject.render(&data, &tera)?;
        let body = body.render(&data, &tera)?;

        Ok((subject, body))
    })
    .await
    .expect("Blocking task panicked")
}

impl From<lettre::message::Mailbox> for Mailbox {
    fn from(value: lettre::message::Mailbox) -> Self {
        Self(value)
    }
}

impl Content {
    pub fn render(self, data: &tera::Context, tera: &tera::Tera) -> Result<String, tera::Error> {
        match self {
            Content::Text { content } => Ok(content),
            Content::Template { template_name } => Ok(tera.render(&template_name, data)?),
        }
    }
}

impl BodyContent {
    pub fn render(self, data: &tera::Context, tera: &tera::Tera) -> Result<EmailBody, tera::Error> {
        match self {
            BodyContent::Plain(content) => Ok(EmailBody::Plain {
                content: content.render(data, tera)?,
            }),
            BodyContent::Html(content) => Ok(EmailBody::Html {
                content: content.render(data, tera)?,
            }),
            BodyContent::Multipart { plain, html } => Ok(EmailBody::Multipart {
                plain: plain.render(data, tera)?,
                html: html.render(data, tera)?,
            }),
        }
    }
}

impl FromStr for Mailbox {
    type Err = MailerError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mailbox = lettre::message::Mailbox::from_str(s)?;
        Ok(Self(mailbox))
    }
}

impl TryFrom<Email> for lettre::Message {
    type Error = lettre::error::Error;

    fn try_from(email: Email) -> Result<Self, Self::Error> {
        let mut message_builder = lettre::Message::builder().subject(email.subject);

        if let Some(from) = email.from {
            message_builder = message_builder.from(from.0);
        }

        if let Some(sender) = email.sender {
            message_builder = message_builder.sender(sender.0);
        }

        for reply_to in email.reply_to {
            message_builder = message_builder.reply_to(reply_to.0);
        }

        for to in email.to {
            message_builder = message_builder.to(to.0);
        }

        for cc in email.cc {
            message_builder = message_builder.cc(cc.0);
        }

        for bcc in email.bcc {
            message_builder = message_builder.bcc(bcc.0);
        }

        let message = match email.body {
            EmailBody::Plain { content } => {
                message_builder.singlepart(lettre::message::SinglePart::plain(content))?
            }
            EmailBody::Html { content } => {
                message_builder.singlepart(lettre::message::SinglePart::html(content))?
            }
            EmailBody::Multipart { plain, html } => message_builder.multipart(
                lettre::message::MultiPart::alternative_plain_html(plain, html),
            )?,
        };

        Ok(message)
    }
}
