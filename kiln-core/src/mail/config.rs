use std::{path::PathBuf, time::Duration};

use lettre::message::Mailbox;
use serde::{Deserialize, Serialize};
use serde_with::DurationSeconds;

/// Configuration for a mail.
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct MailerConfig {
    /// Default *From* header for E-Mails.
    ///
    /// Default: `Kiln No-Reply <no-reply@kiln.com>`
    #[serde(with = "kiln_utils::serde::mailbox", default = "default_from")]
    pub default_from: Mailbox,

    /// Default *ReplyTo* header for E-Mails.
    #[serde(with = "kiln_utils::serde::opt_mailbox", default)]
    pub default_reply_to: Option<Mailbox>,

    /// Transport configuration.
    pub transport: MailerTransportConfig,
}

pub fn default_from() -> Mailbox {
    "Kiln (no-reply) <no-reply@kiln.com>"
        .parse()
        .expect("Invalid mailbox")
}

/// Configuration for the transport of the mail.
///
/// Options: `null` | `file` | `smtp`
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(tag = "kind")]
pub enum MailerTransportConfig {
    /// Makes sent Mails disappear without a trace.
    #[serde(rename = "null")]
    Null,

    /// Writes sent mails as eml files to a directory.
    #[serde(rename = "file")]
    File(FileMailerConfig),

    /// Sends mails to an SMTP relay server.
    #[serde(rename = "smtp")]
    Smtp(SmtpMailerConfig),
}

/// Configuration for an SMTP relay server.
#[serde_with::serde_as]
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct SmtpMailerConfig {
    /// SMTP relay. For example `localhost` or `smtp.gmail.com`.
    pub relay: String,

    /// SMTP TLS mode.
    /// `none` - Insecure plaintext connection
    /// `starttls` - Begin with plaintext connection and require `STARTTLS`
    /// `full` - Start the connection with TLS from the start
    ///
    /// Options: `none` | `starttls` | `full`
    /// Default: `full`
    pub tls_mode: SmtpTlsMode,

    /// SMTP port. Used to override the used port.
    #[serde(default)]
    pub port: Option<u16>,

    /// SMTP credentials.
    #[serde(default)]
    pub credentials: Option<SmtpMailerCredentialConfig>,

    /// Override the EHLO client ID. Tries to use the hostname if not specified.
    #[serde(default)]
    pub hello_name: Option<String>,

    /// Sets a timeout for sending mails in seconds.
    #[serde(default)]
    #[serde_as(as = "Option<DurationSeconds<u64>>")]
    pub timeout: Option<Duration>,

    /// Connection pool configuration.
    #[serde(default)]
    pub pool: SmtpMailerPoolConfig,
}

/// Specified the tls mode.
#[derive(Debug, Clone, Copy, Default, Deserialize, Serialize)]
pub enum SmtpTlsMode {
    #[serde(rename = "none")]
    None,
    #[serde(rename = "starttls")]
    StartTls,
    #[default]
    #[serde(rename = "full")]
    Full,
}

/// SMTP authentication configuration.
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct SmtpMailerCredentialConfig {
    /// User
    pub user: String,

    /// Password
    pub password: String,
}

/// SMTP connection pool configuration.
#[serde_with::serde_as]
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(default)]
pub struct SmtpMailerPoolConfig {
    /// Minimum number of connections.
    ///
    /// Default: `0`
    pub min_connections: u32,

    /// Maximum number of connections.
    ///
    /// Default: `5`
    pub max_connections: u32,

    /// Timeout for closing a connection after being idle in seconds.
    ///
    /// Default: `60`
    #[serde_as(as = "DurationSeconds<u64>")]
    pub idle_timeout: Duration,
}

impl Default for SmtpMailerPoolConfig {
    fn default() -> Self {
        Self {
            min_connections: 0,
            max_connections: 5,
            idle_timeout: Duration::from_secs(60),
        }
    }
}

/// Configuration for a file mail that writes mails as files.
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct FileMailerConfig {
    /// Directory where the EML files are saved.
    pub path: PathBuf,
}
