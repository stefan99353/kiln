use std::sync::Arc;

use parking_lot::RwLock;
use sea_orm::DbConn;

use crate::{database, environment::Environment, error::KilnResult, mail, templates};

/// Represents the kiln state used in routes, task and more.
///
/// Contains various shared resources and settings.
#[derive(Debug, Clone)]
pub struct KilnContext {
    /// The running environment.
    pub _environment: Environment,

    /// The database connection pool.
    pub database: DbConn,

    /// The mail client.
    pub mail_sender: mail::sender::EmailSender,

    /// The tera templating engine.
    pub template_engine: templates::TemplateEngine,
}

impl KilnContext {
    pub async fn create_context(
        environment: Environment,
        database_config: &database::config::DatabaseConfig,
        mailer_config: Option<&mail::config::MailerConfig>,
        tera_config: &templates::config::TemplatesConfig,
    ) -> KilnResult<Self> {
        // Create database connection pool
        let db = database::connect(database_config).await?;

        // Create mail
        let mailer = mail::sender::EmailSender::connect(mailer_config)?;

        // Create templating engine
        let tera = tera::Tera::new(&tera_config.glob)?;

        let ctx = Self {
            _environment: environment,
            database: db,
            mail_sender: mailer,
            template_engine: Arc::new(RwLock::new(tera)),
        };

        Ok(ctx)
    }
}
