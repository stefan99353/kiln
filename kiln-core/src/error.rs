use crate::{mail, models, task};

pub type KilnResult<T> = Result<T, KilnError>;

#[derive(Debug, thiserror::Error)]
pub enum KilnError {
    #[error(transparent)]
    Tasks(#[from] task::error::TasksError),

    #[error(transparent)]
    Mailer(#[from] mail::error::MailerError),

    #[error(transparent)]
    Model(#[from] models::error::ModelError),

    #[error(transparent)]
    Validation(#[from] validator::ValidationErrors),

    #[error(transparent)]
    Tera(#[from] tera::Error),

    #[error(transparent)]
    Database(#[from] sea_orm::DbErr),

    #[error("{0}")]
    Message(String),

    #[error(transparent)]
    Any(Box<dyn std::error::Error + Send + Sync>),
}

impl KilnError {
    pub fn msg(msg: impl Into<String>) -> Self {
        Self::Message(msg.into())
    }

    pub fn any<E>(err: E) -> Self
    where
        E: std::error::Error + Send + Sync + 'static,
    {
        Self::Any(Box::new(err))
    }
}

impl<E> From<sea_orm::TransactionError<E>> for KilnError
where
    E: std::error::Error,
    E: Into<Self>,
{
    fn from(err: sea_orm::TransactionError<E>) -> Self {
        match err {
            sea_orm::TransactionError::Connection(err) => Self::Database(err),
            sea_orm::TransactionError::Transaction(err) => err.into(),
        }
    }
}
