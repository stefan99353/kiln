pub mod config;

use sea_orm::{
    ConnectOptions, ConnectionTrait, Database, DatabaseBackend, DbConn, DbErr, Statement,
};

use crate::database::config::DatabaseConfig;

pub async fn connect(config: &DatabaseConfig) -> Result<DbConn, DbErr> {
    tracing::trace!("Building database connection options");
    let mut options = ConnectOptions::new(&config.uri);

    options
        .min_connections(config.pool.min_connections)
        .max_connections(config.pool.max_connections)
        .connect_timeout(config.pool.connect_timeout)
        .idle_timeout(config.pool.idle_timeout)
        .sqlx_logging(config.enable_logging)
        .sqlx_logging_level(config.logging_level.into())
        .sqlx_slow_statements_logging_settings(
            config.slow_statements_level.into(),
            config.slow_statements_threshold,
        );

    if let Some(acquire_timeout) = config.pool.acquire_timeout {
        options.acquire_timeout(acquire_timeout);
    }

    if let Some(max_lifetime) = config.pool.max_lifetime {
        options.max_lifetime(max_lifetime);
    }

    tracing::trace!(options = ?options, "Starting the database connections");
    let db = Database::connect(options).await?;

    if db.get_database_backend() == DatabaseBackend::Sqlite {
        tracing::trace!("Settings SQLite pragmas for database");
        db.execute(Statement::from_string(
            DatabaseBackend::Sqlite,
            r#"
PRAGMA foreign_keys = ON;
PRAGMA journal_mode = WAL;
PRAGMA synchronous = NORMAL;
"#,
        ))
        .await?;
    }

    Ok(db)
}
