use std::time::Duration;

use serde::{Deserialize, Serialize};
use serde_with::{DurationMilliSeconds, DurationSeconds};

/// Configuration for the database connection.
#[serde_with::serde_as]
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct DatabaseConfig {
    /// The connection string which is used to connect to the database.
    /// - Postgres: `postgres://user:password@localhost:5432/kiln`
    /// - MySQL: `mysql://user:password@localhost:3306/kiln`
    /// - SQLite: `sqlite://database.sqlite?mode=rwc`
    pub uri: String,

    /// Enable `SQLx` statement logging.
    ///
    /// Default: `false`
    #[serde(default)]
    pub enable_logging: bool,

    /// The log level at which statements are logged.
    /// Ignored if `enable_logging` is set to `false`.
    ///
    /// Options: `off` | `trace` | `debug` | `info` | `warn` | `error`
    /// Default: `debug`
    #[serde(default)]
    pub logging_level: LevelFilter,

    /// The log level at which slow statements are logged.
    /// Ignored if `enable_logging` is set to `false`.
    /// Time at which a statement is considered slow is configured with
    /// `slow_statements_level`.
    ///
    /// Options: `off` | `trace` | `debug` | `info` | `warn` | `error`
    /// Default: `off`
    #[serde(default = "default_slow_statements_level")]
    pub slow_statements_level: LevelFilter,

    /// Time at which a statement is considered slow in milliseconds.
    ///
    /// Default: `1000`
    #[serde(default = "default_slow_statements_threshold")]
    #[serde_as(as = "DurationMilliSeconds<u64>")]
    pub slow_statements_threshold: Duration,

    /// Connection pool configuration.
    pub pool: DatabasePoolConfig,
}

fn default_slow_statements_level() -> LevelFilter {
    LevelFilter::Off
}

fn default_slow_statements_threshold() -> Duration {
    Duration::from_millis(1000)
}

/// Configuration for the database connection pool.
#[serde_with::serde_as]
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct DatabasePoolConfig {
    /// Minimum number of connections for a pool.
    pub min_connections: u32,

    /// Maximum number of connections for a pool.
    pub max_connections: u32,

    /// Timeout for establishing a new connection in seconds.
    #[serde_as(as = "DurationSeconds<u64>")]
    pub connect_timeout: Duration,

    /// Timeout for closing a connection after being idle in seconds.
    #[serde_as(as = "DurationSeconds<u64>")]
    pub idle_timeout: Duration,

    /// Timeout for acquiring a connection from the pool in seconds.
    #[serde(default)]
    #[serde_as(as = "Option<DurationSeconds<u64>>")]
    pub acquire_timeout: Option<Duration>,

    /// Maximum lifetime of individual connections.
    #[serde(default)]
    #[serde_as(as = "Option<DurationSeconds<u64>>")]
    pub max_lifetime: Option<Duration>,
}

/// Level for database logs.
#[derive(Debug, Default, Copy, Clone, Deserialize, Serialize)]
pub enum LevelFilter {
    /// Turn off logging.
    #[serde(rename = "off")]
    Off,
    /// Log at error level.
    #[serde(rename = "error")]
    Error,
    /// Log at warn level.
    #[serde(rename = "warn")]
    Warn,
    /// Log at info level.
    #[serde(rename = "info")]
    Info,
    /// Log at debug level.
    #[serde(rename = "debug")]
    #[default]
    Debug,
    /// Log at trace level.
    #[serde(rename = "trace")]
    Trace,
}

impl From<LevelFilter> for log::LevelFilter {
    fn from(value: LevelFilter) -> Self {
        match value {
            LevelFilter::Off => Self::Off,
            LevelFilter::Error => Self::Error,
            LevelFilter::Warn => Self::Warn,
            LevelFilter::Info => Self::Info,
            LevelFilter::Debug => Self::Debug,
            LevelFilter::Trace => Self::Trace,
        }
    }
}
