use std::{collections::HashMap, future::Future, pin::Pin, sync::Arc};

use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::{
    context::KilnContext,
    error::KilnResult,
    task::{error::TasksError, tasks, Task},
};

pub type TaskHandler = Box<
    dyn Fn(Uuid, Option<serde_json::Value>) -> Pin<Box<dyn Future<Output = KilnResult<()>> + Send>>
        + Send
        + Sync,
>;

#[derive(Clone, Default)]
pub struct TaskRegistry {
    handlers: Arc<HashMap<&'static str, TaskHandler>>,
}

impl TaskRegistry {
    pub fn handler(&self, name: &str) -> Option<&TaskHandler> {
        self.handlers.get(name)
    }

    pub fn new(context: KilnContext) -> KilnResult<Self> {
        // Register task here
        let registry =
            Self::default().register_task::<tasks::mailer::MailerTask, _>(context.clone())?;

        Ok(registry)
    }

    fn register_task<T, A>(mut self, context: KilnContext) -> KilnResult<Self>
    where
        T: Task<A>,
        A: Serialize + Send + Sync + 'static,
        for<'de> A: Deserialize<'de>,
    {
        let handler = move |task_id: Uuid, args: Option<serde_json::Value>| {
            let ctx = context.clone();

            Box::pin(async move {
                let args = args
                    .map(|a| serde_json::from_value::<A>(a))
                    .transpose()
                    .map_err(TasksError::DeserializeTaskArgs)?;

                T::run(task_id, args, &ctx).await
            }) as Pin<Box<dyn Future<Output = KilnResult<()>> + Send>>
        };

        Arc::get_mut(&mut self.handlers)
            .expect("Could not mutate handlers")
            .insert(T::name(), Box::new(handler));

        Ok(self)
    }
}
