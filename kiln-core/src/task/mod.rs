pub mod error;
pub mod registry;
pub mod tasks;

use chrono::{NaiveDateTime, Utc};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::{
    context::KilnContext, error::KilnResult, models::queued_task, task::error::TasksError,
};

#[async_trait::async_trait]
pub trait Task<A = ()>
where
    A: Serialize + Send + Sync + 'static,
    for<'de> A: Deserialize<'de>,
{
    fn name() -> &'static str;

    fn priority() -> queued_task::TaskPriority {
        queued_task::TaskPriority::default()
    }

    async fn run(task_id: Uuid, args: Option<A>, ctx: &KilnContext) -> KilnResult<()>;

    async fn schedule(args: Option<A>, ctx: &KilnContext) -> KilnResult<queued_task::Model> {
        Self::schedule_for(args, Utc::now().naive_utc(), ctx).await
    }

    async fn schedule_for(
        args: Option<A>,
        time: NaiveDateTime,
        ctx: &KilnContext,
    ) -> KilnResult<queued_task::Model> {
        let args = args
            .map(|a| serde_json::to_value(&a))
            .transpose()
            .map_err(TasksError::SerializeTaskArgs)?;

        let queued_task = queued_task::Model::enqueue_task(
            Self::name().to_string(),
            Self::priority(),
            args,
            time,
            &ctx.database,
        )
        .await?;

        Ok(queued_task)
    }
}
