use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::{
    context::KilnContext,
    error::KilnResult,
    mail::{
        email::{render_mail, BodyContent, Content, Email},
        message::EmailArgs,
    },
    models::queued_task::TaskPriority,
    task::{error::TasksError, Task},
};

pub struct MailerTask;

#[async_trait::async_trait]
impl Task<MailerTaskArgs> for MailerTask {
    fn name() -> &'static str {
        "mail"
    }

    fn priority() -> TaskPriority {
        TaskPriority::Low
    }

    async fn run(
        _task_id: Uuid,
        args: Option<MailerTaskArgs>,
        ctx: &KilnContext,
    ) -> KilnResult<()> {
        if let Some(email_args) = args {
            let data = tera::Context::from_serialize(&email_args.data)?;
            let (subject, body) =
                render_mail(email_args.subject, email_args.body, data, ctx).await?;

            let email = Email {
                from: email_args.args.from,
                to: email_args.args.to,
                cc: email_args.args.cc,
                bcc: email_args.args.bcc,
                sender: email_args.args.sender,
                reply_to: email_args.args.reply_to,
                subject,
                body,
            };

            ctx.mail_sender.send_mail(email).await?;

            Ok(())
        } else {
            Err(TasksError::MissingTaskArgs.into())
        }
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct MailerTaskArgs {
    pub args: EmailArgs,
    pub subject: Content,
    pub body: BodyContent,
    pub data: serde_json::Value,
}
