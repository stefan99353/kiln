#[derive(Debug, thiserror::Error)]
pub enum TasksError {
    #[error("Task is missing its arguments")]
    MissingTaskArgs,

    #[error(transparent)]
    SerializeTaskArgs(serde_json::Error),

    #[error(transparent)]
    DeserializeTaskArgs(serde_json::Error),

    #[error("Handler registry is not writable, because other references exist")]
    RegistryNotWritable,
}
