use std::time::Duration;

use serde::{Deserialize, Serialize};
use serde_with::DurationSeconds;

/// Configuration for the templates templating engine.
#[serde_with::serde_as]
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct TemplatesConfig {
    /// The directory glob where all templates are located.
    ///
    /// Default: `templates/**/*.tera`
    pub glob: String,

    /// The interval at which the templates are reloaded.
    ///
    /// Default: `None`
    #[serde_as(as = "Option<DurationSeconds<u64>>")]
    pub reload_interval: Option<Duration>,
}

impl Default for TemplatesConfig {
    fn default() -> Self {
        Self {
            glob: "templates/**/*.tera".to_string(),
            reload_interval: None,
        }
    }
}
