use std::sync::Arc;

use parking_lot::RwLock;
use tera::Tera;

pub mod config;

pub type TemplateEngine = Arc<RwLock<Tera>>;
