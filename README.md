# Kiln

TODO:
- [ ] Authentication
  - [ ] Password
  - [ ] Social Login
  - [ ] API Key
    - [ ] Personal Access Tokens
    - [ ] Global API Keys
  - [ ] OAuth2
- [ ] Authorization
- [ ] Request Validation
- [ ] Scheduler
- [ ] Task API
- [ ] Notifications
  - [ ] Mail
  - [ ] WebSocket & API
- [ ] File Storage
- [ ] Caching
