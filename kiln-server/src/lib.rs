mod app;
mod banner;
pub mod cli;
mod config;
mod controllers;
mod error;
mod extractors;
mod middleware;
mod routes;

shadow_rs::shadow!(build);
