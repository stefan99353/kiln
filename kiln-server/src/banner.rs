use kiln_core::environment;

use crate::build;

pub fn print_banner(env: &environment::Environment, config: &crate::config::Config) {
    println!("Application: {}", build::PROJECT_NAME);
    println!("Environment: {}", env);

    if config.logger.enable {
        if let Some(filter) = &config.logger.override_filter {
            println!("Logger:      {}", filter);
        } else {
            println!("Logger:      {}", config.logger.level);
        }
    } else {
        println!("Logger:      disabled");
    }

    if cfg!(debug_assertions) {
        println!("Build Mode:  debug");
    } else {
        println!("Build Mode:  release");
    }
}
