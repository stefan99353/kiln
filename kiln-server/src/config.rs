use std::{
    env,
    net::{IpAddr, Ipv4Addr},
    path::Path,
    time::Duration,
};

use kiln_core::environment;
use serde::{Deserialize, Serialize};
use serde_with::DurationSeconds;

use crate::middleware;

pub const DEFAULT_CONFIG_DIR: &str = "./config";
pub const DEFAULT_CONFIG_FILE_SUFFIX: &str = "server";
pub const SERVER_CONFIG_DIR_VARIABLE: &str = "KILN_SERVER_CONFIG_DIR";
pub const SERVER_CONFIG_SUFFIX_VARIABLE: &str = "KILN_SERVER_CONFIG_SUFFIX";

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Config {
    #[serde(default)]
    pub server: ServerConfig,
    pub database: kiln_core::database::config::DatabaseConfig,
    pub auth: AuthConfig,
    #[serde(default)]
    pub mailer: Option<kiln_core::mail::config::MailerConfig>,
    #[serde(default)]
    pub templates: kiln_core::templates::config::TemplatesConfig,
    #[serde(default)]
    pub misc: MiscConfig,
    #[serde(default)]
    pub logger: kiln_utils::logger::Config,
}

impl Config {
    pub fn api_domain(&self) -> String {
        self.misc
            .api_domain
            .clone()
            .unwrap_or_else(|| format!("http://localhost:{}", self.server.port))
    }
}

/// Configuration for the http server.
///
/// Optional
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(tag = "kind", default)]
pub struct ServerConfig {
    /// The address on which the server listens for incoming connections.
    ///
    /// Default: `127.0.0.1`
    pub binding: IpAddr,

    /// The address on which the server listens for incoming connections.
    ///
    /// Default: `8080`
    pub port: u16,

    /// Sets the value for the `Server` response header.
    pub ident: ServerIdent,

    /// Middleware configuration
    pub middleware: middleware::MiddlewareConfig,
}

/// Server ident for the `Server` response header.
///
/// Options: `none` | `basic` | `detailed` | `custom`
/// Default: `basic`
#[derive(Debug, Clone, Default, Deserialize, Serialize)]
#[serde(tag = "kind")]
pub enum ServerIdent {
    /// Does not add a `Server` header to a response
    #[serde(rename = "none")]
    None,

    /// Adds basic information (`<PROJECT_NAME>`).
    #[default]
    #[serde(rename = "basic")]
    Basic,

    /// Adds detailed information (`Kiln v0.0.0 (<RUST_CHANNEL>)`).
    #[serde(rename = "detailed")]
    Detailed,

    /// Adds customised information.
    #[serde(rename = "custom")]
    Custom {
        /// Custom value for the `Server` header.
        ident: String,
    },
}

impl Default for ServerConfig {
    fn default() -> Self {
        Self {
            binding: IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)),
            port: 8080,
            ident: ServerIdent::default(),
            middleware: middleware::MiddlewareConfig::default(),
        }
    }
}

/// Configuration for everything authentication related.
#[serde_with::serde_as]
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct AuthConfig {
    /// The duration a password reset token is valid for in seconds.
    ///
    /// Default: `3600`
    #[serde_as(as = "DurationSeconds<u64>")]
    pub reset_password_token_expiration: Duration,

    /// The URL of the page for resetting the password.
    /// Used for the E-Mail to create a link.
    /// The reset token is appended as a query parameter named `token`.
    pub reset_password_url: String,
}

/// Configuration for various things.
#[serde_with::serde_as]
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(default)]
pub struct MiscConfig {
    /// The domain where end users can reach the server.
    /// If the server is behind a proxy, this should be the address of the
    /// proxy. Is used for generated links to API endpoints for emails, ...
    ///
    /// Default: `http://localhost:<server.port>`
    pub api_domain: Option<String>,

    /// The default amount of items per paginated request.
    ///
    /// Default: `512`
    pub default_pagination_size: u64,

    /// The maximum amount of items per paginated request.
    /// The server responds with a `400 - BAD_REQUEST` response if the page size
    /// is bigger than this.
    ///
    /// Default: `16_384`
    pub max_pagination_size: u64,

    /// Whether migrations should be run when the server starts.
    /// This can be turned on while developing, but should be turned off in
    /// production.
    ///
    /// Default: `false`
    #[serde(default)]
    pub db_auto_migrate: bool,

    /// Recreates the database when the server starts.
    /// *This drops everything from the database and deletes all data.*
    ///
    /// Default: `false`
    #[serde(default)]
    pub db_dangerously_recreate: bool,
}

impl Default for MiscConfig {
    fn default() -> Self {
        Self {
            api_domain: None,
            default_pagination_size: 512,
            max_pagination_size: 16_384,
            db_auto_migrate: false,
            db_dangerously_recreate: false,
        }
    }
}

impl Config {
    pub fn load(env: &environment::Environment) -> anyhow::Result<Self> {
        let config_path =
            env::var(SERVER_CONFIG_DIR_VARIABLE).unwrap_or_else(|_| DEFAULT_CONFIG_DIR.to_string());

        Self::load_from_dir(env, config_path)
    }

    pub fn load_from_dir(
        env: &environment::Environment,
        path: impl AsRef<Path>,
    ) -> anyhow::Result<Self> {
        let suffix = env::var(SERVER_CONFIG_SUFFIX_VARIABLE)
            .unwrap_or_else(|_| DEFAULT_CONFIG_FILE_SUFFIX.to_string());

        let config_file_name = match suffix.is_empty() {
            true => format!("{env}.toml"),
            false => format!("{env}_{suffix}.toml"),
        };

        let config_file = path.as_ref().join(config_file_name);

        let config_builder = config::Config::builder()
            .add_source(config::File::from(config_file).required(false))
            .add_source(config::Environment::with_prefix("kiln").separator("."));

        let config = config_builder.build()?.try_deserialize()?;

        Ok(config)
    }
}
