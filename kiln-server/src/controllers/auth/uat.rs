use axum::{
    extract::{Query, State},
    response::IntoResponse,
    routing::{delete, get, patch, post},
    Json,
};
use kiln_core::{context::KilnContext, models::user_access_token};
use sea_orm::{ColumnTrait, EntityTrait, QueryFilter, QueryOrder};

use crate::{
    controllers::auth::dtos::UserUuidData,
    error::ResponseResult,
    extractors::{pagination::PaginationQuery, requester::Requester},
    routes::Routes,
};

async fn get_token_list(
    Query(user_uuid): Query<UserUuidData>,
    pagination_query: PaginationQuery,
    requester: Requester,
    State(ctx): State<KilnContext>,
) -> ResponseResult<impl IntoResponse> {
    let user_uuid = match (user_uuid.user_uuid, requester) {
        (Some(uuid), _requester) => {
            // TODO: Permission check
            uuid
        }
        (None, Requester::User(user)) => user.uuid,
    };

    let query = user_access_token::Entity::find()
        .filter(user_access_token::Column::UserUuid.eq(user_uuid))
        .order_by_asc(user_access_token::Column::Uuid);

    // TODO: Don't return token itself
    
    let result = pagination_query.paginate(query, &ctx.database).await?;

    Ok(Json(result))
}

async fn create_token() -> ResponseResult<impl IntoResponse> {
    Ok(())
}

async fn update_token() -> ResponseResult<impl IntoResponse> {
    Ok(())
}

async fn delete_token() -> ResponseResult<impl IntoResponse> {
    Ok(())
}

async fn regenerate_token() -> ResponseResult<impl IntoResponse> {
    Ok(())
}

pub fn routes() -> Routes {
    Routes::at("/uat")
        .add("/list", get(get_token_list))
        .add("/", post(create_token))
        .add("/{uuid}", patch(update_token))
        .add("/{uuid}", delete(delete_token))
        .add("/{uuid}/regenerate", post(regenerate_token))
}
