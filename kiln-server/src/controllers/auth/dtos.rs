use kiln_core::auth;
use serde::Deserialize;
use uuid::Uuid;
use validator::Validate;

#[derive(Debug, Clone, Deserialize, Validate)]
pub struct TokenData {
    #[validate(length(equal = 128))]
    pub token: String,
}

#[derive(Debug, Clone, Deserialize, Validate)]
pub struct ForgotData {
    #[validate(email)]
    pub email: String,
}

#[derive(Debug, Clone, Deserialize, Validate)]
pub struct ResetData {
    #[validate(length(equal = 128))]
    pub token: String,
    #[validate(
        length(min = 8, max = 64),
        custom(function = "auth::utils::validate_password")
    )]
    pub password: String,
}

#[derive(Debug, Clone, Deserialize)]
pub struct UserUuidData {
    pub user_uuid: Option<Uuid>,
}
