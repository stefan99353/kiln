mod dtos;
mod uat;

use axum::{
    extract::State,
    http::StatusCode,
    response::IntoResponse,
    routing::{get, post},
    Json,
};
use kiln_core::{
    context::KilnContext,
    error::KilnError,
    mail,
    mail::message::{EmailArgs, EmailMessage},
    models::{
        error::ModelError, reset_password_token, user, user::RegisterData, user_verify_token,
    },
};
use sea_orm::{EntityName, IntoActiveModel, ModelTrait};

use crate::{
    controllers::auth::dtos::{ForgotData, ResetData, TokenData},
    error::{KilnErrorResponse, ResponseResult},
    extractors::{
        config::ConfigExtractor,
        requester::{Requester, RequesterResponse},
        validation::{json::ValidateJson, query::ValidateQuery},
    },
    routes::Routes,
};

async fn register(
    State(ctx): State<KilnContext>,
    config: ConfigExtractor,
    ValidateJson(params): ValidateJson<RegisterData>,
) -> ResponseResult<impl IntoResponse> {
    let (user, verify_token) = user::Model::create_with_password(params, &ctx.database).await?;

    let mail = mail::messages::verify_user::VerifyUser {
        handle: user.handle.clone(),
        first_name: user.first_name.clone(),
        last_name: user.last_name.clone(),
        domain: config.api_domain(),
        verify_token: verify_token.token,
    };

    mail.send(
        EmailArgs {
            to: vec![user.email.parse()?],
            ..Default::default()
        },
        &ctx,
    )
    .await?;

    Ok(())
}

async fn verify_user(
    State(ctx): State<KilnContext>,
    ValidateQuery(token): ValidateQuery<TokenData>,
) -> ResponseResult<impl IntoResponse> {
    let verify_token = user_verify_token::Model::find_by_token(&token.token, &ctx.database)
        .await?
        .ok_or_else(|| {
            ModelError::EntityNotFound(user_verify_token::Entity.table_name().to_string())
        })?;

    if !verify_token.active {
        return Err(KilnErrorResponse {
            status_code: StatusCode::BAD_REQUEST,
            message: "Token is not valid".to_string(),
            error: None,
        });
    }

    let user = verify_token
        .find_related(user::Entity)
        .one(&ctx.database)
        .await
        .map_err(KilnError::from)?
        .ok_or_else(|| ModelError::EntityNotFound(user::Entity.table_name().to_string()))?;

    verify_token
        .into_active_model()
        .deactivate(&ctx.database)
        .await?;

    if !user.verified {
        user.into_active_model().verify(&ctx.database).await?;
    }

    Ok(())
}

async fn forgot_password(
    State(ctx): State<KilnContext>,
    config: ConfigExtractor,
    ValidateJson(params): ValidateJson<ForgotData>,
) -> ResponseResult<impl IntoResponse> {
    let user = match user::Model::find_by_email(&params.email, &ctx.database).await? {
        None => {
            // Return success even if the email is not found
            return Ok(());
        }
        Some(user) => user,
    };

    let token = reset_password_token::Model::create_token(
        user.uuid,
        config.auth.reset_password_token_expiration,
        &ctx.database,
    )
    .await?;

    let mail = mail::messages::forgot_password::ForgotPassword {
        handle: user.handle.clone(),
        domain: config.auth.reset_password_url.clone(),
        reset_token: token.token,
        expires_at: token.expires_at,
    };

    mail.send(
        EmailArgs {
            to: vec![user.email.parse()?],
            ..Default::default()
        },
        &ctx,
    )
    .await?;

    Ok(())
}

async fn reset_password(
    State(ctx): State<KilnContext>,
    ValidateJson(params): ValidateJson<ResetData>,
) -> ResponseResult<impl IntoResponse> {
    let reset_token = reset_password_token::Model::find_by_token(&params.token, &ctx.database)
        .await?
        .ok_or_else(|| {
            ModelError::EntityNotFound(reset_password_token::Entity.table_name().to_string())
        })?;

    let user = reset_token
        .find_related(user::Entity)
        .one(&ctx.database)
        .await
        .map_err(KilnError::from)?
        .ok_or_else(|| ModelError::EntityNotFound(user::Entity.table_name().to_string()))?;

    user.into_active_model()
        .reset_password(params.password, reset_token, &ctx.database)
        .await?;

    Ok(())
}

async fn current_authenticated(requester: Requester) -> ResponseResult<impl IntoResponse> {
    let resp: RequesterResponse = requester.into();

    Ok(Json(resp))
}

pub fn routes() -> Routes {
    Routes::at("/auth")
        .add("/register", post(register))
        .add("/verify_user", get(verify_user))
        .add("/forgot_password", post(forgot_password))
        .add("/reset_password", post(reset_password))
        .add("/self", get(current_authenticated))
        .nest(uat::routes())
}
