use axum::routing::get;

use crate::routes::Routes;

pub fn routes() -> Routes {
    Routes::new().add("/ping", get(ping))
}

async fn ping() {}
