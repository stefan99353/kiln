use axum::{extract::State, response::IntoResponse, routing::get, Json};
use kiln_core::context::KilnContext;
use serde::Serialize;

use crate::routes::Routes;

pub fn routes() -> Routes {
    Routes::new().add("/health", get(health))
}

#[derive(Debug, Serialize)]
struct Health {
    pub server: bool,
    pub database: bool,
}

async fn health(State(ctx): State<KilnContext>) -> impl IntoResponse {
    let db_ok = match ctx.database.ping().await {
        Ok(()) => true,
        Err(err) => {
            tracing::error!(err = %err, "Error while pinging database");
            false
        }
    };

    Json(Health {
        server: true,
        database: db_ok,
    })
}
