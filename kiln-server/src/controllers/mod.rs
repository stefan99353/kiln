mod auth;
mod health;
mod ping;

use crate::routes::Routes;

pub fn routes() -> Vec<Routes> {
    vec![ping::routes(), health::routes(), auth::routes()]
}
