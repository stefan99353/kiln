use axum::{routing::MethodRouter, Router};
use kiln_core::context::KilnContext;

/// Holds the handlers with an optional prefix.
#[derive(Debug, Clone, Default)]
pub struct Routes {
    /// Prefix where the handlers get mounted.
    pub prefix: Option<String>,
    /// Handlers.
    pub handlers: Vec<Handler>,
    /// Sub routes.
    pub sub_routes: Vec<Routes>,
}

/// A single handler.
#[derive(Debug, Clone, Default)]
pub struct Handler {
    /// The URI of the method.
    pub uri: String,
    /// The method itself.
    pub method: MethodRouter<KilnContext>,
}

impl Routes {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn at(prefix: &str) -> Self {
        Self {
            prefix: Some(prefix.to_string()),
            ..Self::default()
        }
    }

    pub fn add(mut self, uri: &str, method: MethodRouter<KilnContext>) -> Self {
        self.handlers.push(Handler {
            uri: uri.to_string(),
            method,
        });

        self
    }

    pub fn nest(mut self, routes: Routes) -> Self {
        self.sub_routes.push(routes);

        self
    }

    pub fn mount(self, mut router: Router<KilnContext>) -> Router<KilnContext> {
        match self.prefix {
            None => {
                // Handlers
                for handler in self.handlers {
                    router = router.route(&handler.uri, handler.method);
                }

                // Sub routes
                for routes in self.sub_routes {
                    router = routes.mount(router);
                }
            }
            Some(prefix) => {
                let mut nest_router = Router::new();

                // Handlers
                for handler in self.handlers {
                    nest_router = nest_router.route(&handler.uri, handler.method);
                }

                // Sub routes
                for routes in self.sub_routes {
                    nest_router = routes.mount(nest_router);
                }

                router = router.nest(&prefix, nest_router);
            }
        }

        router
    }
}
