use std::time::Duration;

use axum::Router;
use kiln_core::context::KilnContext;
use serde::{Deserialize, Serialize};
use tower_http::cors::{Any, CorsLayer};

use crate::middleware::MiddlewareLayer;

const DEFAULT_ALLOW_ORIGINS: [&str; 1] = ["*"];
const DEFAULT_ALLOW_HEADERS: [&str; 1] = ["*"];
const DEFAULT_ALLOW_METHODS: [&str; 1] = ["*"];
const DEFAULT_VARY_HEADERS: [&str; 3] = [
    "origin",
    "access-control-request-method",
    "access-control-request-headers",
];

/// CORS middleware.
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(default)]
pub struct CorsMiddleware {
    /// Whether this middleware is enabled.
    ///
    /// Default: `false`
    pub enable: bool,

    /// Allow origins.
    ///
    /// Default: `["*"]`
    pub allow_origins: Vec<String>,

    /// Allow headers.
    ///
    /// Default: `["*"]`
    pub allow_headers: Vec<String>,

    /// Allow methods.
    ///
    /// Default: `["*"]`
    pub allow_methods: Vec<String>,

    /// Allow credentials.
    ///
    /// Default: `false`
    pub allow_credentials: bool,

    /// Max age in seconds.
    pub max_age: Option<u64>,

    /// Vary headers.
    ///
    /// Default: `["origin", "access-control-request-method",
    /// "access-control-request-headers"]`
    pub vary: Vec<String>,
}

impl Default for CorsMiddleware {
    fn default() -> Self {
        Self {
            enable: false,
            allow_origins: DEFAULT_ALLOW_ORIGINS
                .iter()
                .map(|s| s.to_string())
                .collect(),
            allow_headers: DEFAULT_ALLOW_HEADERS
                .iter()
                .map(|s| s.to_string())
                .collect(),
            allow_methods: DEFAULT_ALLOW_METHODS
                .iter()
                .map(|s| s.to_string())
                .collect(),
            allow_credentials: false,
            max_age: None,
            vary: DEFAULT_VARY_HEADERS.iter().map(|s| s.to_string()).collect(),
        }
    }
}

impl MiddlewareLayer for CorsMiddleware {
    fn name(&self) -> &'static str {
        "cors"
    }

    fn enabled(&self) -> bool {
        self.enable
    }

    fn apply(&self, router: Router<KilnContext>) -> anyhow::Result<Router<KilnContext>> {
        let mut cors = CorsLayer::new();

        // Origins
        if self.allow_origins == DEFAULT_ALLOW_ORIGINS {
            cors = cors.allow_origin(Any);
        } else {
            let mut origins = vec![];

            for origin in &self.allow_origins {
                origins.push(origin.parse()?);
            }

            if !origins.is_empty() {
                cors = cors.allow_origin(origins);
            }
        }

        // Headers
        if self.allow_headers == DEFAULT_ALLOW_HEADERS {
            cors = cors.allow_headers(Any);
        } else {
            let mut headers = vec![];

            for header in &self.allow_headers {
                headers.push(header.parse()?);
            }

            if !headers.is_empty() {
                cors = cors.allow_headers(headers);
            }
        }

        // Methods
        if self.allow_methods == DEFAULT_ALLOW_METHODS {
            cors = cors.allow_methods(Any);
        } else {
            let mut methods = vec![];

            for method in &self.allow_methods {
                methods.push(method.parse()?);
            }

            if !methods.is_empty() {
                cors = cors.allow_methods(methods);
            }
        }

        // Vary
        let mut list = vec![];

        for v in &self.vary {
            list.push(v.parse()?);
        }

        if !list.is_empty() {
            cors = cors.vary(list);
        }

        // Max Age
        if let Some(max_age) = self.max_age {
            cors = cors.max_age(Duration::from_secs(max_age));
        }

        // Allow Credentials
        cors = cors.allow_credentials(self.allow_credentials);

        Ok(router.layer(cors))
    }
}
