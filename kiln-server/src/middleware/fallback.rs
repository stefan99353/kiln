use std::path::PathBuf;

use axum::{http::StatusCode, Router};
use kiln_core::context::KilnContext;
use serde::{Deserialize, Serialize};
use tower_http::services::ServeFile;

use crate::middleware::MiddlewareLayer;

/// Fallback middleware.
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(default)]
pub struct FallbackMiddleware {
    /// Whether this middleware is enabled.
    ///
    /// Default: `false`
    pub enable: bool,

    /// Status code that is returned with the response.
    ///
    /// Default: `404`
    #[serde(with = "kiln_utils::serde::status_code")]
    pub status_code: StatusCode,

    /// Returns the content of a file along with `status_code`.
    /// When set, takes priority over `not_found`.
    pub file: Option<PathBuf>,

    /// Returns a message along with `status_code`.
    pub not_found: Option<String>,
}

impl Default for FallbackMiddleware {
    fn default() -> Self {
        Self {
            enable: false,
            status_code: StatusCode::NOT_FOUND,
            file: None,
            not_found: None,
        }
    }
}

impl MiddlewareLayer for FallbackMiddleware {
    fn name(&self) -> &'static str {
        "fallback"
    }

    fn enabled(&self) -> bool {
        self.enable
    }

    fn apply(&self, router: Router<KilnContext>) -> anyhow::Result<Router<KilnContext>> {
        if let Some(file) = &self.file {
            let serve_file = ServeFile::new(file);
            Ok(router.fallback_service(serve_file))
        } else if let Some(message) = &self.not_found {
            let message = message.clone();
            let status_code = self.status_code;
            Ok(router.fallback(move || async move { (status_code, message) }))
        } else {
            let status_code = self.status_code;
            Ok(router.fallback(move || async move { status_code }))
        }
    }
}
