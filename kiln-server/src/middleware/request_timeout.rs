use std::time::Duration;

use axum::Router;
use kiln_core::context::KilnContext;
use serde::{Deserialize, Serialize};
use tower_http::timeout::TimeoutLayer;

use crate::middleware::MiddlewareLayer;

/// Request timeout middleware.
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(default)]
pub struct RequestTimeoutMiddleware {
    /// Whether this middleware is enabled.
    ///
    /// Default: `false`
    pub enable: bool,

    /// Request timeout in milliseconds.
    ///
    /// Default: `5000`
    pub timeout: u64,
}

impl Default for RequestTimeoutMiddleware {
    fn default() -> Self {
        Self {
            enable: false,
            timeout: 5_000,
        }
    }
}

impl MiddlewareLayer for RequestTimeoutMiddleware {
    fn name(&self) -> &'static str {
        "request-timeout"
    }

    fn enabled(&self) -> bool {
        self.enable
    }

    fn apply(&self, router: Router<KilnContext>) -> anyhow::Result<Router<KilnContext>> {
        let layer = TimeoutLayer::new(Duration::from_millis(self.timeout));

        Ok(router.layer(layer))
    }
}
