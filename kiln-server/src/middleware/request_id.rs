use std::sync::LazyLock;

use axum::{extract::Request, http::HeaderValue, middleware::Next, response::Response, Router};
use kiln_core::context::KilnContext;
use regex::Regex;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::middleware::MiddlewareLayer;

const X_REQUEST_ID: &str = "x-request-id";
const MAX_LEN: usize = 255;

static ID_CLEANUP_REGEX: LazyLock<Regex> =
    LazyLock::new(|| Regex::new(r"[^\w\-@]").expect("Invalid regex pattern"));

/// Request ID middleware.
#[derive(Debug, Clone, Default, Deserialize, Serialize)]
#[serde(default)]
pub struct RequestIdMiddleware {
    /// Whether this middleware is enabled.
    ///
    /// Default: `false`
    pub enable: bool,
}

impl MiddlewareLayer for RequestIdMiddleware {
    fn name(&self) -> &'static str {
        "request-id"
    }

    fn enabled(&self) -> bool {
        self.enable
    }

    fn apply(&self, router: Router<KilnContext>) -> anyhow::Result<Router<KilnContext>> {
        Ok(router.layer(axum::middleware::from_fn(request_id_middleware)))
    }
}

/// Wrapper struct for the request ID from the headers.
#[derive(Debug, Clone)]
pub struct KilnRequestId(String);

impl KilnRequestId {
    /// Get the value as a string slice.
    pub fn get(&self) -> &str {
        &self.0
    }
}

/// Middleware function that ensures a request gets an ID.
///
/// This is done by either using an existing header and sanitizing it, or by
/// creating a new ID. In addition to setting the response header, it adds the
/// ID as an extension for easier access.
pub async fn request_id_middleware(mut request: Request, next: Next) -> Response {
    let header_value = request.headers().get(X_REQUEST_ID).cloned();

    let request_id = make_request_id(header_value);

    request
        .extensions_mut()
        .insert(KilnRequestId(request_id.clone()));

    let mut response = next.run(request).await;

    if let Ok(val) = HeaderValue::from_str(&request_id) {
        response.headers_mut().insert(X_REQUEST_ID, val);
    } else {
        tracing::warn!("Could not set request ID into the response headers: '{request_id}'");
    }

    response
}

/// Sanitizes an ID or creates a new one.
fn make_request_id(maybe_value: Option<HeaderValue>) -> String {
    maybe_value
        .and_then(|val| {
            let id: Option<String> = val.to_str().ok().map(|s| {
                ID_CLEANUP_REGEX
                    .replace_all(s, "")
                    .chars()
                    .take(MAX_LEN)
                    .collect()
            });

            id.filter(|s| !s.is_empty())
        })
        .unwrap_or_else(|| Uuid::new_v4().to_string())
}
