use axum::{http, Router};
use kiln_core::context::KilnContext;
use serde::{Deserialize, Serialize};
use tower_http::trace::TraceLayer;

use crate::middleware::{request_id::KilnRequestId, MiddlewareLayer};

/// Logger middleware.
#[derive(Debug, Clone, Default, Deserialize, Serialize)]
#[serde(default)]
pub struct LoggerMiddleware {
    /// Whether this middleware is enabled.
    ///
    /// Default: `false`
    pub enable: bool,
}

impl MiddlewareLayer for LoggerMiddleware {
    fn name(&self) -> &'static str {
        "logger"
    }

    fn enabled(&self) -> bool {
        self.enable
    }

    fn apply(&self, router: Router<KilnContext>) -> anyhow::Result<Router<KilnContext>> {
        let layer = TraceLayer::new_for_http().make_span_with(|request: &http::Request<_>| {
            let request_id = request
                .extensions()
                .get::<KilnRequestId>()
                .map_or_else(|| "req-id-none".to_string(), |id| id.get().to_string());

            let user_agent = request
                .headers()
                .get(http::header::USER_AGENT)
                .map_or("", |val| val.to_str().unwrap_or(""));

            tracing::error_span!(
                "http-request",
                "http.method" = display(request.method()),
                "http.uri" = display(request.uri()),
                "http.version" = debug(request.version()),
                "http.user_agent" = display(user_agent),
                "request_id" = display(request_id),
            )
        });

        Ok(router.layer(layer))
    }
}
