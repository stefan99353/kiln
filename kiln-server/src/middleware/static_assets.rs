use std::path::PathBuf;

use anyhow::bail;
use axum::Router;
use kiln_core::context::KilnContext;
use serde::{Deserialize, Serialize};
use tower_http::services::{ServeDir, ServeFile};

use crate::middleware::MiddlewareLayer;

/// Static asset's middleware.
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(default)]
pub struct StaticAssetsMiddleware {
    /// Whether this middleware is enabled.
    ///
    /// Default: `false`
    pub enable: bool,

    /// Whether assets from `folder.path` and `fallback` must exist.
    ///
    /// Default: `true`
    pub must_exist: bool,

    /// Assets location.
    pub folder: FolderConfig,

    /// Fallback file for when no asset exists.
    /// Useful for SPAs where routes are virtual.
    ///
    /// Default: `assets/static/404.html`
    pub fallback: PathBuf,

    /// Enable possibility to serve pre-compressed GZIP files.
    ///
    /// Default: `false`
    pub precompressed_gzip: bool,

    /// Enable possibility to serve pre-compressed DEFLATE files.
    ///
    /// Default: `false`
    pub precompressed_deflate: bool,

    /// Enable possibility to serve pre-compressed BROTLI files.
    ///
    /// Default: `false`
    pub precompressed_br: bool,

    /// Enable possibility to serve pre-compressed ZSTD files.
    ///
    /// Default: `false`
    pub precompressed_zstd: bool,
}

impl Default for StaticAssetsMiddleware {
    fn default() -> Self {
        Self {
            enable: false,
            must_exist: true,
            folder: FolderConfig::default(),
            fallback: PathBuf::from("assets/static/404.html"),
            precompressed_gzip: false,
            precompressed_deflate: false,
            precompressed_br: false,
            precompressed_zstd: false,
        }
    }
}

/// Configuration for assets location.
#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct FolderConfig {
    /// URI where the assets are served.
    ///
    /// Default: `/static`
    pub uri: String,

    /// Location of the assets on disk.
    ///
    /// Default: `assets/static`
    pub path: PathBuf,
}

impl Default for FolderConfig {
    fn default() -> Self {
        Self {
            uri: "/static".to_string(),
            path: PathBuf::from("assets/static"),
        }
    }
}

impl MiddlewareLayer for StaticAssetsMiddleware {
    fn name(&self) -> &'static str {
        "static-assets"
    }

    fn enabled(&self) -> bool {
        self.enable
    }

    fn apply(&self, router: Router<KilnContext>) -> anyhow::Result<Router<KilnContext>> {
        if self.must_exist {
            if !self.folder.path.exists() {
                bail!(
                    "The static assets path does not exist: '{}'",
                    self.folder.path.display()
                );
            }
            if !self.fallback.exists() {
                bail!(
                    "The static assets fallback path does not exist: '{}'",
                    self.fallback.display()
                );
            }
        }

        let mut serve_dir = ServeDir::new(&self.folder.path);
        let mut serve_file = ServeFile::new(&self.fallback);

        if self.precompressed_gzip {
            serve_dir = serve_dir.precompressed_gzip();
            serve_file = serve_file.precompressed_gzip();
        }

        if self.precompressed_deflate {
            serve_dir = serve_dir.precompressed_deflate();
            serve_file = serve_file.precompressed_deflate();
        }

        if self.precompressed_br {
            serve_dir = serve_dir.precompressed_br();
            serve_file = serve_file.precompressed_br();
        }

        if self.precompressed_zstd {
            serve_dir = serve_dir.precompressed_zstd();
            serve_file = serve_file.precompressed_zstd();
        }

        let serve_dir = serve_dir.fallback(serve_file);

        match &self.folder.uri == "/" {
            true => Ok(router.fallback_service(serve_dir)),
            false => Ok(router.nest_service(&self.folder.uri, serve_dir)),
        }
    }
}
