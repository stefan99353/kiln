mod catch_panic;
mod compression;
mod cors;
mod fallback;
mod limit_payload;
mod logger;
mod request_id;
mod request_timeout;
mod static_assets;

use axum::Router;
use kiln_core::context::KilnContext;
use serde::{Deserialize, Serialize};

/// Trait that represents an axum middleware layer.
pub trait MiddlewareLayer {
    /// Name of this middleware.
    fn name(&self) -> &'static str;

    /// Whether this layer is enabled.
    fn enabled(&self) -> bool {
        true
    }

    /// Applies this middleware to the given axum Router and returns the
    /// modified one.
    fn apply(&self, router: Router<KilnContext>) -> anyhow::Result<Router<KilnContext>>;
}

/// Configuration for the server middleware.
#[derive(Debug, Clone, Default, Deserialize, Serialize)]
#[serde(default)]
pub struct MiddlewareConfig {
    /// Response compression.
    pub compression: Option<compression::CompressionMiddleware>,

    /// Limit payload size.
    pub limit_payload: Option<limit_payload::LimitPayloadMiddleware>,

    /// Logger with request id and more request data.
    pub logger: Option<logger::LoggerMiddleware>,

    /// Catch code panics and return an error.
    pub catch_panic: Option<catch_panic::CatchPanicMiddleware>,

    /// Request timeout.
    pub request_timeout: Option<request_timeout::RequestTimeoutMiddleware>,

    /// CORS configuration.
    pub cors: Option<cors::CorsMiddleware>,

    /// Serving of static assets.
    pub static_assets: Option<static_assets::StaticAssetsMiddleware>,

    /// Fallback behaviour for unknown URLs.
    pub fallback: Option<fallback::FallbackMiddleware>,

    /// Request ID.
    pub request_id: Option<request_id::RequestIdMiddleware>,
}

pub fn middleware_stack(middleware_config: &MiddlewareConfig) -> Vec<Box<dyn MiddlewareLayer>> {
    vec![
        Box::new(middleware_config.limit_payload.clone().unwrap_or_default()),
        Box::new(middleware_config.cors.clone().unwrap_or_default()),
        Box::new(middleware_config.catch_panic.clone().unwrap_or_default()),
        Box::new(middleware_config.compression.clone().unwrap_or_default()),
        Box::new(
            middleware_config
                .request_timeout
                .clone()
                .unwrap_or_default(),
        ),
        Box::new(middleware_config.static_assets.clone().unwrap_or_default()),
        Box::new(middleware_config.logger.clone().unwrap_or_default()),
        Box::new(middleware_config.request_id.clone().unwrap_or_default()),
        Box::new(middleware_config.fallback.clone().unwrap_or_default()),
    ]
}
