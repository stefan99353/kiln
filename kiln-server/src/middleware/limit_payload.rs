use axum::{extract::DefaultBodyLimit, Router};
use byte_unit::Byte;
use kiln_core::context::KilnContext;
use serde::{Deserialize, Serialize};

use crate::middleware::MiddlewareLayer;

/// Limit payload middleware.
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(default)]
pub struct LimitPayloadMiddleware {
    /// Whether this middleware is enabled.
    /// When this middleware is *NOT* enabled, a default limit of 2MB is applied
    /// by default.
    ///
    /// Default: `false`
    pub enable: bool,

    /// Disables the request size limit when the middleware is enabled.
    ///
    /// Default: `false`
    pub disable_limit: bool,

    /// The maximum amount of bytes a request can have.
    ///
    /// Default: `2 MB`
    pub limit: Byte,
}

impl Default for LimitPayloadMiddleware {
    fn default() -> Self {
        Self {
            enable: false,
            disable_limit: false,
            limit: Byte::from(2_000_000u32),
        }
    }
}

impl MiddlewareLayer for LimitPayloadMiddleware {
    fn name(&self) -> &'static str {
        "limit-payload"
    }

    fn enabled(&self) -> bool {
        self.enable
    }

    fn apply(&self, router: Router<KilnContext>) -> anyhow::Result<Router<KilnContext>> {
        let layer = match self.disable_limit {
            true => DefaultBodyLimit::disable(),
            false => DefaultBodyLimit::max(self.limit.as_u64() as usize),
        };

        Ok(router.layer(layer))
    }
}
