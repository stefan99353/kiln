use axum::Router;
use kiln_core::context::KilnContext;
use serde::{Deserialize, Serialize};
use tower_http::compression::CompressionLayer;

use crate::middleware::MiddlewareLayer;

/// Compression middleware.
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(default)]
pub struct CompressionMiddleware {
    /// Whether this middleware is enabled.
    ///
    /// Default: `false`
    pub enable: bool,

    /// Use GZIP encoding when enabled.
    ///
    /// Default: `true`
    pub gzip: bool,

    /// Use DEFLATE encoding when enabled.
    ///
    /// Default: `true`
    pub deflate: bool,

    /// Use BROTLI encoding when enabled.
    ///
    /// Default: `true`
    pub br: bool,

    /// Use ZSTD encoding when enabled.
    ///
    /// Default: `true`
    pub zstd: bool,
}

impl Default for CompressionMiddleware {
    fn default() -> Self {
        Self {
            enable: false,
            gzip: true,
            deflate: true,
            br: true,
            zstd: true,
        }
    }
}

impl MiddlewareLayer for CompressionMiddleware {
    fn name(&self) -> &'static str {
        "compression"
    }

    fn enabled(&self) -> bool {
        self.enable
    }

    fn apply(&self, router: Router<KilnContext>) -> anyhow::Result<Router<KilnContext>> {
        let layer = CompressionLayer::new()
            .gzip(self.gzip)
            .deflate(self.deflate)
            .br(self.br)
            .zstd(self.zstd);

        Ok(router.layer(layer))
    }
}
