use axum::Router;
use kiln_core::context::KilnContext;
use serde::{Deserialize, Serialize};
use tower_http::catch_panic::CatchPanicLayer;

use crate::middleware::MiddlewareLayer;

/// Catch panic middleware.
#[derive(Debug, Clone, Default, Deserialize, Serialize)]
#[serde(default)]
pub struct CatchPanicMiddleware {
    /// Whether this middleware is enabled.
    ///
    /// Default: `false`
    pub enable: bool,
}

impl MiddlewareLayer for CatchPanicMiddleware {
    fn name(&self) -> &'static str {
        "catch-panic"
    }

    fn enabled(&self) -> bool {
        self.enable
    }

    fn apply(&self, router: Router<KilnContext>) -> anyhow::Result<Router<KilnContext>> {
        let layer = CatchPanicLayer::new();

        Ok(router.layer(layer))
    }
}
