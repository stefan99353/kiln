mod from;

use axum::{
    http::StatusCode,
    response::{IntoResponse, Response},
    Json,
};
use serde::{Serialize, Serializer};

pub type ResponseResult<T> = Result<T, KilnErrorResponse>;

#[derive(Debug, Serialize)]
pub struct KilnErrorResponse {
    #[serde(skip)]
    pub status_code: StatusCode,
    pub message: String,
    #[cfg_attr(not(debug_assertions), serde(skip))]
    #[cfg_attr(debug_assertions, serde(skip_serializing_if = "Option::is_none"))]
    #[serde(serialize_with = "serialize_error")]
    pub error: Option<Box<dyn std::error::Error>>,
}

impl IntoResponse for KilnErrorResponse {
    fn into_response(self) -> Response {
        if (500..600).contains(&self.status_code.as_u16()) {
            match &self.error {
                None => tracing::error!(self.message),
                Some(err) => tracing::error!(err = %err, self.message),
            }
        }

        (self.status_code, Json(self)).into_response()
    }
}

#[cfg_attr(not(debug_assertions), allow(dead_code))]
fn serialize_error<S>(error: &Option<Box<dyn std::error::Error>>, ser: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    let error = error.as_ref().map(|e| e.to_string());
    error.serialize(ser)
}
