use axum::http::StatusCode;
use kiln_core::{
    error::KilnError, mail::error::MailerError, models::error::ModelError, task::error::TasksError,
};

use crate::error::KilnErrorResponse;

impl From<KilnError> for KilnErrorResponse {
    fn from(error: KilnError) -> Self {
        match error {
            KilnError::Tasks(err) => err.into(),
            KilnError::Mailer(err) => err.into(),
            KilnError::Model(err) => err.into(),
            KilnError::Validation(err) => Self {
                status_code: StatusCode::BAD_REQUEST,
                message: "Invalid data provided".to_string(),
                error: Some(Box::new(err)),
            },
            KilnError::Tera(err) => Self {
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                message: "Failure while working with templates".to_string(),
                error: Some(Box::new(err)),
            },
            KilnError::Database(err) => Self {
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                message: "Failure while interacting with the database".to_string(),
                error: Some(Box::new(err)),
            },
            KilnError::Message(err) => Self {
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                message: err,
                error: None,
            },
            KilnError::Any(err) => Self {
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                message: "Unknown error".to_string(),
                error: Some(err),
            },
        }
    }
}

impl From<TasksError> for KilnErrorResponse {
    fn from(error: TasksError) -> Self {
        match error {
            TasksError::MissingTaskArgs => Self {
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                message: "Failure while executing task, arguments are missing".to_string(),
                error: Some(Box::new(error)),
            },
            TasksError::SerializeTaskArgs(err) => Self {
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                message: "Failure while serializing the arguments for a task".to_string(),
                error: Some(Box::new(err)),
            },
            TasksError::DeserializeTaskArgs(err) => Self {
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                message: "Failure while executing task, could not deserialize arguments"
                    .to_string(),
                error: Some(Box::new(err)),
            },
            TasksError::RegistryNotWritable => Self {
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                message: "Failure while registering task handler".to_string(),
                error: Some(Box::new(error)),
            },
        }
    }
}

impl From<MailerError> for KilnErrorResponse {
    fn from(error: MailerError) -> Self {
        match error {
            MailerError::MailerNotConfigured => Self {
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                message: "Mailer is not configured".to_string(),
                error: Some(Box::new(error)),
            },
            MailerError::Lettre(err) => Self {
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                message: "Failure while working with emails".to_string(),
                error: Some(Box::new(err)),
            },
            MailerError::LettreAddress(err) => Self {
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                message: "Failure while parsing email addresses".to_string(),
                error: Some(Box::new(err)),
            },
            MailerError::LettreSmtp(err) => Self {
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                message: "Failure while interacting with an SMTP server".to_string(),
                error: Some(Box::new(err)),
            },
            MailerError::LettreFile(err) => Self {
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                message: "Failure while storing emails as files".to_string(),
                error: Some(Box::new(err)),
            },
            MailerError::SerializeRenderCtx(err) => Self {
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                message: "Failure while serializing email render context".to_string(),
                error: Some(Box::new(err)),
            },
        }
    }
}

impl From<ModelError> for KilnErrorResponse {
    fn from(error: ModelError) -> Self {
        match error {
            ModelError::EntityExists => Self {
                status_code: StatusCode::BAD_REQUEST,
                message: "The entity already exists".to_string(),
                error: Some(Box::new(error)),
            },
            ModelError::EntityNotFound(model) => Self {
                status_code: StatusCode::NOT_FOUND,
                message: "The entity could not be found".to_string(),
                error: Some(Box::new(ModelError::EntityNotFound(model))),
            },
        }
    }
}
