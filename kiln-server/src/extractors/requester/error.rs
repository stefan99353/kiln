use axum::http::StatusCode;

use crate::error::KilnErrorResponse;

#[derive(Debug, thiserror::Error)]
pub enum ExtractRequesterError {
    #[error("The user is not known to the system")]
    UnknownUser,

    #[error("The user does not have a password configured")]
    UserNoPassword,

    #[error("The user is not active")]
    UserInactive,

    #[error("The user is not verified")]
    UserNotVerified,

    #[error("The users password does not match the saved hash")]
    WrongPassword,

    #[error("The 'Authorization' header is missing")]
    MissingHeader,

    #[error("'Authorization' header has an invalid format (<scheme> <content>)")]
    InvalidHeaderFormat,

    #[error("Unsupported auth scheme '{0}'")]
    UnsupportedAuthScheme(String),

    #[error("Basic authentication content is missing the delimiter ':'")]
    MissingBasicDelimiter,

    #[error(transparent)]
    Base64Decode(#[from] base64::DecodeError),

    #[error(transparent)]
    InvalidHeaderChars(#[from] axum::http::header::ToStrError),

    #[error(transparent)]
    InvalidEncodedCharacters(#[from] std::string::FromUtf8Error),

    #[error(transparent)]
    PasswordHash(#[from] password_hash::Error),
}

impl From<ExtractRequesterError> for KilnErrorResponse {
    fn from(error: ExtractRequesterError) -> Self {
        match error {
            ExtractRequesterError::UnknownUser
            | ExtractRequesterError::UserNoPassword
            | ExtractRequesterError::UserInactive
            | ExtractRequesterError::UserNotVerified
            | ExtractRequesterError::WrongPassword => Self {
                status_code: StatusCode::UNAUTHORIZED,
                message: "Invalid user and/or password".to_string(),
                error: Some(Box::new(error)),
            },
            ExtractRequesterError::MissingHeader => Self {
                status_code: StatusCode::UNAUTHORIZED,
                message: "Missing authorization header".to_string(),
                error: Some(Box::new(error)),
            },
            ExtractRequesterError::InvalidHeaderFormat
            | ExtractRequesterError::UnsupportedAuthScheme(_)
            | ExtractRequesterError::MissingBasicDelimiter => Self {
                status_code: StatusCode::BAD_REQUEST,
                message: "Invalid format of authorization header".to_string(),
                error: Some(Box::new(error)),
            },
            ExtractRequesterError::Base64Decode(err) => Self {
                status_code: StatusCode::BAD_REQUEST,
                message: "Invalid format of authorization header".to_string(),
                error: Some(Box::new(err)),
            },
            ExtractRequesterError::InvalidHeaderChars(err) => Self {
                status_code: StatusCode::BAD_REQUEST,
                message: "Invalid format of authorization header".to_string(),
                error: Some(Box::new(err)),
            },
            ExtractRequesterError::InvalidEncodedCharacters(err) => Self {
                status_code: StatusCode::BAD_REQUEST,
                message: "Invalid format of authorization header".to_string(),
                error: Some(Box::new(err)),
            },
            ExtractRequesterError::PasswordHash(err) => Self {
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                message: "Password error".to_string(),
                error: Some(Box::new(err)),
            },
        }
    }
}
