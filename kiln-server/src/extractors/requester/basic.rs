use base64::{prelude::BASE64_STANDARD, Engine};
use kiln_core::{auth::utils::verify_password, context::KilnContext, models::user};

use crate::{
    error::KilnErrorResponse,
    extractors::requester::{error::ExtractRequesterError, Requester},
};

pub async fn authenticate(
    content: &str,
    ctx: &KilnContext,
) -> Result<Requester, KilnErrorResponse> {
    let decoded_content = BASE64_STANDARD
        .decode(content.as_bytes())
        .map_err(ExtractRequesterError::Base64Decode)?;

    let content_string = String::from_utf8(decoded_content)
        .map_err(ExtractRequesterError::InvalidEncodedCharacters)?;

    let (username, password) = content_string
        .split_once(':')
        .ok_or(ExtractRequesterError::MissingBasicDelimiter)?;
    let password = password.to_string();

    let user = user::Model::find_by_email(username, &ctx.database)
        .await?
        .ok_or(ExtractRequesterError::UnknownUser)?;

    let requester = match (user.active, user.verified, user.password_hash.clone()) {
        (false, _, _) => Err(ExtractRequesterError::UserInactive),
        (true, false, _) => Err(ExtractRequesterError::UserNotVerified),
        (true, true, None) => Err(ExtractRequesterError::UserNoPassword),
        (true, true, Some(hash)) => {
            let password_matches =
                tokio::task::spawn_blocking(move || verify_password(&password, &hash))
                    .await
                    .expect("Verifying password panicked")
                    .map_err(ExtractRequesterError::PasswordHash)?;

            match password_matches {
                true => Ok(Requester::User(user)),
                false => Err(ExtractRequesterError::WrongPassword),
            }
        }
    }?;

    Ok(requester)
}
