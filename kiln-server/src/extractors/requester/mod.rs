mod basic;
mod error;

use axum::{
    extract::FromRequestParts,
    http::{header, request::Parts, HeaderMap},
};
use kiln_core::{context::KilnContext, models::user};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::{error::KilnErrorResponse, extractors::requester::error::ExtractRequesterError};

const BASIC_AUTH_SCHEME: &str = "basic";

#[derive(Debug, Clone)]
pub enum Requester {
    User(user::Model),
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct RequesterResponse {
    pub uuid: Uuid,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub email: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
}

impl From<&Requester> for RequesterResponse {
    fn from(value: &Requester) -> Self {
        match value {
            Requester::User(user) => Self {
                uuid: user.uuid,
                email: Some(user.email.clone()),
                name: user.name(),
            },
        }
    }
}

impl From<Requester> for RequesterResponse {
    fn from(value: Requester) -> Self {
        (&value).into()
    }
}

impl FromRequestParts<KilnContext> for Requester {
    type Rejection = KilnErrorResponse;

    async fn from_request_parts(
        parts: &mut Parts,
        state: &KilnContext,
    ) -> Result<Self, Self::Rejection> {
        let headers = HeaderMap::from_request_parts(parts, state)
            .await
            .expect("Infallible");

        let auth_header = headers
            .get(header::AUTHORIZATION)
            .ok_or(ExtractRequesterError::MissingHeader)?
            .to_str()
            .map_err(ExtractRequesterError::InvalidHeaderChars)?;

        let (auth_scheme, auth_content) = auth_header
            .split_once(' ')
            .ok_or(ExtractRequesterError::InvalidHeaderFormat)?;

        match auth_scheme.to_lowercase().as_str() {
            BASIC_AUTH_SCHEME => basic::authenticate(auth_content, state).await,
            scheme => Err(ExtractRequesterError::UnsupportedAuthScheme(scheme.to_string()).into()),
        }
    }
}
