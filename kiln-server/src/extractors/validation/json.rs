use std::ops::Deref;

use axum::{
    extract::{FromRequest, Request},
    response::{IntoResponse, Response},
    Json,
};
use kiln_core::error::KilnError;
use serde::de::DeserializeOwned;
use validator::Validate;

use crate::error::KilnErrorResponse;

#[derive(Debug, Clone, Copy, Default)]
pub struct ValidateJson<T>(pub T);

impl<T, S> FromRequest<S> for ValidateJson<T>
where
    T: DeserializeOwned + Validate,
    S: Send + Sync,
{
    type Rejection = Response;

    async fn from_request(req: Request, state: &S) -> Result<Self, Self::Rejection> {
        let data: Json<T> = Json::from_request(req, state)
            .await
            .map_err(IntoResponse::into_response)?;

        data.validate()
            .map_err(|e| KilnErrorResponse::from(KilnError::Validation(e)).into_response())?;

        Ok(Self(data.0))
    }
}

impl<T> Deref for ValidateJson<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
