use std::ops::Deref;

use axum::{
    extract::{FromRequestParts, Path},
    http::request::Parts,
    response::{IntoResponse, Response},
};
use kiln_core::error::KilnError;
use serde::de::DeserializeOwned;
use validator::Validate;

use crate::error::KilnErrorResponse;

#[derive(Debug, Clone, Copy, Default)]
pub struct ValidatePath<T>(pub T);

impl<T, S> FromRequestParts<S> for ValidatePath<T>
where
    T: DeserializeOwned + Validate + Send,
    S: Send + Sync,
{
    type Rejection = Response;

    async fn from_request_parts(parts: &mut Parts, state: &S) -> Result<Self, Self::Rejection> {
        let data: Path<T> = Path::from_request_parts(parts, state)
            .await
            .map_err(IntoResponse::into_response)?;

        data.validate()
            .map_err(|e| KilnErrorResponse::from(KilnError::Validation(e)).into_response())?;

        Ok(Self(data.0))
    }
}

impl<T> Deref for ValidatePath<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
