use std::ops::Deref;

use axum::{
    extract::{FromRequestParts, Query},
    http::request::Parts,
    response::{IntoResponse, Response},
};
use kiln_core::error::KilnError;
use serde::de::DeserializeOwned;
use validator::Validate;

use crate::error::KilnErrorResponse;

#[derive(Debug, Clone, Copy, Default)]
pub struct ValidateQuery<T>(pub T);

impl<T, S> FromRequestParts<S> for ValidateQuery<T>
where
    T: DeserializeOwned + Validate,
    S: Send + Sync,
{
    type Rejection = Response;

    async fn from_request_parts(parts: &mut Parts, state: &S) -> Result<Self, Self::Rejection> {
        let data: Query<T> = Query::from_request_parts(parts, state)
            .await
            .map_err(IntoResponse::into_response)?;

        data.validate()
            .map_err(|e| KilnErrorResponse::from(KilnError::Validation(e)).into_response())?;

        Ok(Self(data.0))
    }
}

impl<T> Deref for ValidateQuery<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
