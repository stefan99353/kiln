use std::{ops::Deref, sync::Arc};

use axum::{
    extract::{rejection::ExtensionRejection, FromRequestParts},
    http::request::Parts,
    Extension,
};

use crate::config::Config;

pub struct ConfigExtractor(Arc<Config>);

impl<S> FromRequestParts<S> for ConfigExtractor
where
    S: Send + Sync,
{
    type Rejection = ExtensionRejection;

    async fn from_request_parts(parts: &mut Parts, state: &S) -> Result<Self, Self::Rejection> {
        let config: Extension<Arc<Config>> = Extension::from_request_parts(parts, state).await?;

        Ok(Self(config.0.clone()))
    }
}

impl Deref for ConfigExtractor {
    type Target = Config;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
