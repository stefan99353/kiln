use axum::{
    extract::{FromRequestParts, Query},
    http::{request::Parts, StatusCode},
    response::{IntoResponse, Response},
};
use kiln_core::error::KilnResult;
use sea_orm::{ConnectionTrait, EntityTrait, FromQueryResult, PaginatorTrait, Select};
use serde::{Deserialize, Serialize};

use crate::{error::KilnErrorResponse, extractors::config::ConfigExtractor};

#[derive(Debug, Clone, Copy, Deserialize, Serialize)]
pub struct PaginationQuery {
    pub page_size: u64,
    pub page: u64,
}

#[derive(Debug, Clone, Copy, Default, Deserialize, Serialize)]
#[serde(default)]
struct PaginationParams {
    pub page_size: Option<u64>,
    pub page: Option<u64>,
}

impl<S> FromRequestParts<S> for PaginationQuery
where
    S: Send + Sync,
{
    type Rejection = Response;

    async fn from_request_parts(parts: &mut Parts, state: &S) -> Result<Self, Self::Rejection> {
        let config = ConfigExtractor::from_request_parts(parts, state)
            .await
            .map_err(IntoResponse::into_response)?;

        let params: Query<PaginationParams> = Query::from_request_parts(parts, state)
            .await
            .map_err(IntoResponse::into_response)?;

        let page_size = params
            .page_size
            .unwrap_or(config.misc.default_pagination_size);
        let page = params.page.unwrap_or(0);

        if !(0..=config.misc.max_pagination_size).contains(&page_size) {
            return Err(KilnErrorResponse {
                status_code: StatusCode::BAD_REQUEST,
                message: format!(
                    "Page size must be in the range of ({:?})",
                    0..=config.misc.max_pagination_size
                ),
                error: None,
            }
            .into_response());
        }

        Ok(Self { page_size, page })
    }
}

impl PaginationQuery {
    pub async fn paginate<'db, C, M, E>(self, select: Select<E>, db: &'db C) -> KilnResult<Page<M>>
    where
        C: ConnectionTrait,
        E: EntityTrait<Model = M>,
        M: FromQueryResult + Sized + Send + Sync + 'db,
    {
        let paginator = select.paginate(db, self.page_size);
        let counts = paginator.num_items_and_pages().await?;
        let items = paginator.fetch_page(self.page).await?;

        let result = Page {
            page: self.page,
            page_size: self.page_size,
            page_count: counts.number_of_pages,
            item_count: counts.number_of_items,
            items,
        };

        Ok(result)
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Page<T> {
    pub page: u64,
    pub page_size: u64,
    pub page_count: u64,
    pub item_count: u64,
    pub items: Vec<T>,
}
