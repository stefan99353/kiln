use std::net::IpAddr;

use clap::{ArgAction, Parser, Subcommand};
use kiln_core::environment;
use kiln_utils::logger;

use crate::{
    app::{Application, ServeParams},
    banner, build, config,
};

#[derive(Debug, Parser)]
#[command(author, version, about, long_about = None)]
#[command(propagate_version = true)]
struct Cli {
    #[command(subcommand)]
    command: Commands,

    /// Specify the environment
    #[arg(short, long, global = true)]
    environment: Option<environment::Environment>,
}

#[derive(Debug, Subcommand)]
enum Commands {
    #[clap(alias("s"))]
    Start {
        /// Server bind address
        #[arg(short, long, action)]
        binding: Option<IpAddr>,

        /// Server listen port
        #[arg(short, long, action)]
        port: Option<u16>,

        /// Disable printing the banner
        #[arg(short, long, action = ArgAction::SetTrue)]
        no_banner: bool,
    },

    Version,
}

pub async fn run() -> anyhow::Result<()> {
    // Load environment and config
    let cli = Cli::parse();
    let environment = cli
        .environment
        .unwrap_or_else(environment::Environment::resolve_from_env);

    let config = config::Config::load(&environment)?;

    // Init logger
    logger::init(&config.logger)?;

    // Print config
    tracing::trace!(environment = %environment, "Environment");
    tracing::trace!(config = ?config, "Config");

    match cli.command {
        Commands::Start {
            binding,
            port,
            no_banner,
        } => {
            if !no_banner {
                banner::print_banner(&environment, &config);
            }

            let serve_params = ServeParams {
                binding: binding.unwrap_or(config.server.binding),
                port: port.unwrap_or(config.server.port),
            };

            let app = Application::new(environment, config).await?;

            app.boot(serve_params).await?;
        }
        Commands::Version => {
            if cfg!(debug_assertions) {
                println!(
                    "[DEBUG] {} - {} ({})",
                    build::PROJECT_NAME,
                    build::PKG_VERSION,
                    build::SHORT_COMMIT
                );
            } else {
                println!(
                    "{} - {} ({})",
                    build::PROJECT_NAME,
                    build::PKG_VERSION,
                    build::SHORT_COMMIT
                );
            }
        }
    }

    Ok(())
}
