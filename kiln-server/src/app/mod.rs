use std::{
    net::{IpAddr, SocketAddr},
    sync::Arc,
};

use axum::{
    http::{header, HeaderValue},
    Extension, Router,
};
use kiln_core::{context::KilnContext, environment::Environment};

use crate::{
    config::{Config, ServerIdent},
    controllers, middleware,
};

#[derive(Debug)]
pub struct ServeParams {
    pub binding: IpAddr,
    pub port: u16,
}

#[derive(Debug)]
pub struct Application {
    pub router: Router,
    pub context: KilnContext,
    pub config: Config,
}

impl Application {
    async fn serve(self, serve_params: ServeParams) -> anyhow::Result<()> {
        let bind_address = SocketAddr::new(serve_params.binding, serve_params.port);
        tracing::info!("Starting server on {}", &bind_address);
        let listener = tokio::net::TcpListener::bind(bind_address).await?;

        axum::serve(
            listener,
            self.router
                .into_make_service_with_connect_info::<SocketAddr>(),
        )
        .with_graceful_shutdown(async move {
            kiln_utils::shutdown::shutdown_signal().await;
            tracing::info!("Gracefully shutting down...");
        })
        .await?;

        Ok(())
    }

    pub async fn boot(self, serve_params: ServeParams) -> anyhow::Result<()> {
        // Converge database
        kiln_migrations::converge(
            self.config.misc.db_auto_migrate,
            self.config.misc.db_dangerously_recreate,
            &self.context.database,
        )
        .await?;

        // Start listening
        self.serve(serve_params).await?;

        Ok(())
    }

    pub async fn new(environment: Environment, config: Config) -> anyhow::Result<Self> {
        let context = KilnContext::create_context(
            environment,
            &config.database,
            config.mailer.as_ref(),
            &config.templates,
        )
        .await?;

        // Template reloading
        kiln_utils::templates::start_reload_interval(
            context.template_engine.clone(),
            config.templates.reload_interval,
        );

        // Mount routes
        let mut api_router = Router::new();
        for routes in controllers::routes() {
            api_router = routes.mount(api_router);
        }

        let mut root_router = Router::new();
        root_router = root_router.nest("/api", api_router);

        // Apply middlewares
        let middlewares = middleware::middleware_stack(&config.server.middleware)
            .into_iter()
            .filter(|m| m.enabled())
            .collect::<Vec<_>>();

        for middleware in middlewares {
            root_router = middleware.apply(root_router)?;
            tracing::debug!(name = middleware.name(), "Applying middleware");
        }

        match &config.server.ident {
            ServerIdent::None => {}
            ServerIdent::Basic => {
                let layer = tower_http::set_header::SetResponseHeaderLayer::if_not_present(
                    header::SERVER,
                    HeaderValue::from_static(crate::build::PROJECT_NAME),
                );

                root_router = root_router.layer(layer);
            }
            ServerIdent::Detailed => {
                let layer = tower_http::set_header::SetResponseHeaderLayer::if_not_present(
                    header::SERVER,
                    HeaderValue::from_str(&format!(
                        "{} v{} ({})",
                        crate::build::PROJECT_NAME,
                        crate::build::PKG_VERSION,
                        crate::build::RUST_CHANNEL
                    ))?,
                );

                root_router = root_router.layer(layer);
            }
            ServerIdent::Custom { ident } => {
                let layer = tower_http::set_header::SetResponseHeaderLayer::if_not_present(
                    header::SERVER,
                    HeaderValue::from_str(ident)?,
                );

                root_router = root_router.layer(layer);
            }
        }

        // Apply state and config
        let router = root_router
            .layer(Extension(Arc::new(config.clone())))
            .with_state(context.clone());

        Ok(Self {
            router,
            context,
            config,
        })
    }
}
