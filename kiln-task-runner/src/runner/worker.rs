use std::sync::atomic::Ordering;

use kiln_core::task::registry::TaskRegistry;
use sea_orm::{DbConn, IntoActiveModel};
use tokio::task::JoinHandle;
use uuid::Uuid;

use crate::{
    config::RunnerConfig,
    runner::{Shutdown, TaskQueue},
};

#[tracing::instrument(
    name = "worker",
    level = "debug",
    skip_all,
    fields (
        worker_uuid = %_worker_uuid,
    ),
)]
async fn worker_loop(
    _worker_uuid: Uuid,
    task_queue: TaskQueue,
    registry: TaskRegistry,
    config: RunnerConfig,
    db: DbConn,
    shutdown: Shutdown,
) {
    loop {
        tracing::trace!("Checking queue for task to process");
        let maybe_task = task_queue.lock().pop_front();

        if let Some(task) = maybe_task {
            let task_uuid = task.uuid;

            tracing::trace!(task.uuid = %task_uuid, "Marking task as running in database");
            let task = match task.into_active_model().start(&db).await {
                Ok(task) => task,
                Err(err) => {
                    tracing::error!(task.uuid = %task_uuid, err = %err, "Could not mark task as running");
                    continue;
                }
            };

            // Get handler for this task by name
            if let Some(handler) = registry.handler(&task.name) {
                tracing::debug!(task.uuid = %task_uuid, "Running task");
                let run_result = handler(task.uuid, task.args.clone()).await;

                match run_result {
                    Ok(()) => {
                        tracing::trace!(task.uuid = %task_uuid, "Marking task as complete in database");
                        if let Err(err) = task.into_active_model().complete(&db).await {
                            tracing::error!(task.uuid = %task_uuid, err = %err, "Could not mark task as completed");
                            continue;
                        };
                    }
                    Err(err) => {
                        tracing::error!(task.uuid = %task_uuid, err = %err, "Task failed");
                        tracing::trace!(task.uuid = %task_uuid, "Marking task as failed in database");
                        if let Err(err) = task.into_active_model().fail(&db).await {
                            tracing::error!(task.uuid = %task_uuid, err = %err, "Could not mark task as failed");
                            continue;
                        };
                    }
                }
            } else {
                tracing::error!(task.uuid = %task_uuid, task.name = task.name, "Missing task handler");
                tracing::trace!(task.uuid = %task_uuid, "Marking task as failed in database");
                if let Err(err) = task.into_active_model().fail(&db).await {
                    tracing::error!(task.uuid = %task_uuid, err = %err, "Could not mark task as failed");
                    continue;
                };
            }
        } else {
            tracing::trace!("Checking if shutdown flag is set");
            if shutdown.load(Ordering::SeqCst) {
                tracing::trace!("Shutdown flag is set, finishing worker loop");
                break;
            }

            tracing::trace!(
                interval = config.worker_poll_interval.as_millis(),
                "Sleeping worker thread for configured poll interval"
            );
            tokio::time::sleep(config.worker_poll_interval).await;
        }

        tracing::debug!("Worker loop finished");
    }
}

pub fn start_workers(
    task_queue: TaskQueue,
    registry: TaskRegistry,
    config: RunnerConfig,
    db: DbConn,
    shutdown: Shutdown,
) -> Vec<JoinHandle<()>> {
    let mut handles = vec![];

    tracing::info!(amount = config.num_task_workers, "Starting workers");
    for _ in 0..config.num_task_workers {
        let worker_uuid = Uuid::new_v4();
        let task_queue = task_queue.clone();
        let registry = registry.clone();
        let config = config.clone();
        let db = db.clone();
        let shutdown = shutdown.clone();

        tracing::debug!(uuid = %worker_uuid, "Starting worker");
        let handle = tokio::spawn(worker_loop(
            worker_uuid,
            task_queue,
            registry,
            config,
            db,
            shutdown,
        ));

        handles.push(handle)
    }

    handles
}
