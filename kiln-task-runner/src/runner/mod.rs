mod manager;
mod worker;

use std::{
    collections::VecDeque,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
};

use kiln_core::{
    context::KilnContext, environment::Environment, models::queued_task,
    task::registry::TaskRegistry,
};
use kiln_utils::shutdown::shutdown_signal;
use parking_lot::Mutex;
use sea_orm::DbConn;
use tokio::task::JoinHandle;

use crate::config::{Config, RunnerConfig};

pub type TaskQueue = Arc<Mutex<VecDeque<queued_task::Model>>>;
pub type Shutdown = Arc<AtomicBool>;

pub struct TaskRunner {
    registry: TaskRegistry,
    queue: TaskQueue,
    config: RunnerConfig,
    database: DbConn,
    shutdown: Shutdown,
}

impl TaskRunner {
    pub async fn run(self) -> anyhow::Result<()> {
        let shutdown = self.shutdown.clone();
        let shutdown_timeout = self.config.shutdown_timeout;

        // Start manager and workers
        let handle = self.start();

        // Wait for shutdown signal
        shutdown_signal().await;

        // Shutdown manager and workers
        tracing::trace!("Received shutdown signal, informing manager and worker");
        shutdown.store(true, Ordering::SeqCst);

        // Wait for shutdown
        println!("Press Ctrl+C again to force quit");
        tokio::select! {
            _ = handle => {}
            _ = tokio::time::sleep(shutdown_timeout) => {tracing::warn!("Timeout reached, killing workers")}
            () = shutdown_signal() => {tracing::warn!("Killing workers")}
        }

        Ok(())
    }

    fn start(self) -> JoinHandle<()> {
        tokio::spawn(async move {
            // Start manager and workers
            tracing::trace!("Starting manager and worker task");
            let manager_handle = manager::start_manager(
                self.queue.clone(),
                self.config.clone(),
                self.database.clone(),
                self.shutdown.clone(),
            );
            let worker_handles = worker::start_workers(
                self.queue,
                self.registry,
                self.config,
                self.database,
                self.shutdown,
            );

            // Wait for manager and workers to finish
            manager_handle.await.unwrap();
            for handle in worker_handles {
                handle.await.unwrap();
            }
        })
    }

    pub async fn new(environment: Environment, config: Config) -> anyhow::Result<Self> {
        tracing::trace!("Building application context");
        let context = KilnContext::create_context(
            environment,
            &config.database,
            config.mailer.as_ref(),
            &config.templates,
        )
        .await?;

        // Template reloading
        kiln_utils::templates::start_reload_interval(
            context.template_engine.clone(),
            config.templates.reload_interval,
        );

        let database = context.database.clone();

        tracing::trace!("Building task registry containing all registered handlers");
        let task_registry = TaskRegistry::new(context)?;

        Ok(Self {
            registry: task_registry,
            queue: Arc::new(Mutex::new(VecDeque::new())),
            config: config.runner,
            database,
            shutdown: Arc::new(AtomicBool::new(false)),
        })
    }
}
