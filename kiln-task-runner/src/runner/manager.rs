use std::sync::atomic::Ordering;

use kiln_core::models::queued_task;
use sea_orm::DbConn;
use tokio::task::JoinHandle;
use uuid::Uuid;

use crate::{
    config::RunnerConfig,
    runner::{Shutdown, TaskQueue},
};

#[tracing::instrument(
    name = "manager",
    level = "debug",
    skip_all,
    fields (
        manager_uuid = %_manager_uuid,
    ),
)]
async fn manager_loop(
    _manager_uuid: Uuid,
    task_queue: TaskQueue,
    config: RunnerConfig,
    db: DbConn,
    shutdown: Shutdown,
) {
    loop {
        tracing::trace!("Checking if shutdown flag is set");
        if shutdown.load(Ordering::SeqCst) {
            tracing::trace!("Shutdown flag is set, finishing manager loop");
            break;
        }

        tracing::trace!(
            interval = config.manager_poll_interval.as_millis(),
            "Sleeping manager thread for configured poll interval"
        );
        tokio::time::sleep(config.manager_poll_interval).await;

        tracing::trace!("Checking the internal queue size");
        let queue_len = task_queue.lock().len();
        if queue_len >= config.buffer_size {
            tracing::trace!(
                len = queue_len,
                size = config.buffer_size,
                "Internal queue is full"
            );
            continue;
        }

        // Get task and update their status
        let available_queue_space = config.buffer_size - queue_len;
        tracing::debug!(amount = available_queue_space, "Dequeue task from database");
        let queued_tasks =
            match queued_task::Model::dequeue_tasks(available_queue_space as u64, &db).await {
                Ok(tasks) => tasks,
                Err(err) => {
                    tracing::error!(err = %err, "Could not dequeue task from database");
                    continue;
                }
            };

        if !queued_tasks.is_empty() {
            tracing::trace!(
                amount = queued_tasks.len(),
                "Pushing task to internal queue"
            );
            task_queue.lock().extend(queued_tasks);
        }
    }

    tracing::debug!("Manager loop finished");
}

pub fn start_manager(
    task_queue: TaskQueue,
    config: RunnerConfig,
    db: DbConn,
    shutdown: Shutdown,
) -> JoinHandle<()> {
    let manager_uuid = Uuid::new_v4();
    tracing::debug!(uuid = %manager_uuid, "Starting manager");
    tokio::spawn(manager_loop(manager_uuid, task_queue, config, db, shutdown))
}
