fn main() -> anyhow::Result<()> {
    // Load .env file
    dotenvy::dotenv().ok();

    kiln_task_runner::cli::run()
}
