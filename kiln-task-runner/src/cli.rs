use clap::{ArgAction, Parser, Subcommand};
use kiln_core::environment;
use kiln_utils::logger;

use crate::{banner, build, config, runner::TaskRunner};

#[derive(Debug, Parser)]
#[command(author, version, about, long_about = None)]
#[command(propagate_version = true)]
struct Cli {
    #[command(subcommand)]
    command: Commands,

    /// Specify the environment
    #[arg(short, long, global = true)]
    environment: Option<environment::Environment>,
}

#[derive(Debug, Subcommand)]
enum Commands {
    #[clap(alias("s"))]
    Start {
        /// Disable printing the banner
        #[arg(short, long, action = ArgAction::SetTrue)]
        no_banner: bool,
    },

    Version,
}

async fn run_async(
    cli: Cli,
    environment: environment::Environment,
    config: config::Config,
) -> anyhow::Result<()> {
    match cli.command {
        Commands::Start { no_banner } => {
            if !no_banner {
                banner::print_banner(&environment, &config);
            }

            tracing::trace!("Creating the runner instance");
            let runner = TaskRunner::new(environment, config).await?;

            tracing::trace!("Running the task runner instance");
            runner.run().await?;
        }
        Commands::Version => {
            if cfg!(debug_assertions) {
                println!(
                    "[DEBUG] {} - {} ({})",
                    build::PROJECT_NAME,
                    build::PKG_VERSION,
                    build::SHORT_COMMIT
                );
            } else {
                println!(
                    "{} - {} ({})",
                    build::PROJECT_NAME,
                    build::PKG_VERSION,
                    build::SHORT_COMMIT
                );
            }
        }
    }

    Ok(())
}

pub fn run() -> anyhow::Result<()> {
    // Load environment and config
    let cli = Cli::parse();
    let environment = cli
        .environment
        .clone()
        .unwrap_or_else(environment::Environment::resolve_from_env);

    let config = config::Config::load(&environment)?;

    // Init logger
    logger::init(&config.logger)?;

    // Print config
    tracing::trace!(environment = %environment, "Environment");
    tracing::trace!(config = ?config, "Config");

    // Build tokio runtime
    let mut tokio_builder = tokio::runtime::Builder::new_multi_thread();

    tokio_builder
        .enable_all()
        .build()?
        .block_on(run_async(cli, environment, config))?;

    Ok(())
}
