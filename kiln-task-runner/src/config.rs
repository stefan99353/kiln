use std::{env, path::Path, time::Duration};

use kiln_core::environment;
use serde::{Deserialize, Serialize};
use serde_with::{DurationMilliSeconds, DurationSeconds};

pub const DEFAULT_CONFIG_DIR: &str = "./config";
pub const DEFAULT_CONFIG_FILE_SUFFIX: &str = "runner";
pub const RUNNER_CONFIG_DIR_VARIABLE: &str = "KILN_RUNNER_CONFIG_DIR";
pub const RUNNER_CONFIG_SUFFIX_VARIABLE: &str = "KILN_RUNNER_CONFIG_SUFFIX";

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct Config {
    #[serde(default)]
    pub runner: RunnerConfig,
    pub database: kiln_core::database::config::DatabaseConfig,
    #[serde(default)]
    pub mailer: Option<kiln_core::mail::config::MailerConfig>,
    #[serde(default)]
    pub templates: kiln_core::templates::config::TemplatesConfig,
    #[serde(default)]
    pub logger: kiln_utils::logger::Config,
}

/// Configuration for the task runner
#[serde_with::serde_as]
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(default)]
pub struct RunnerConfig {
    /// The number of workers that run task.
    ///
    /// Default: `10`
    pub num_task_workers: usize,

    /// The polling interval for checking for new tasks in database in
    /// milliseconds.
    ///
    /// Default: `1000`
    #[serde_as(as = "DurationMilliSeconds<u64>")]
    pub manager_poll_interval: Duration,

    /// The polling interval for checking for pending tasks in internal queue in
    /// milliseconds.
    ///
    /// Default: `1000`
    #[serde_as(as = "DurationMilliSeconds<u64>")]
    pub worker_poll_interval: Duration,

    /// The internal queue size where workers take the task.
    ///
    /// Default: `num_task_workers * 2`
    pub buffer_size: usize,

    /// The timeout after which running task are killed when shutting down in
    /// seconds.
    ///
    /// Default: `60`
    #[serde_as(as = "DurationSeconds<u64>")]
    pub shutdown_timeout: Duration,

    /// The number of threads tokio uses for its workers.
    ///
    /// Default: Number of CPU cores
    pub tokio_threads: Option<usize>,
}

impl Default for RunnerConfig {
    fn default() -> Self {
        Self {
            num_task_workers: 10,
            manager_poll_interval: Duration::from_millis(1000),
            worker_poll_interval: Duration::from_millis(1000),
            buffer_size: 20,
            shutdown_timeout: Duration::from_secs(60),
            tokio_threads: None,
        }
    }
}

impl Config {
    pub fn load(env: &environment::Environment) -> anyhow::Result<Self> {
        let config_path =
            env::var(RUNNER_CONFIG_DIR_VARIABLE).unwrap_or_else(|_| DEFAULT_CONFIG_DIR.to_string());

        Self::load_from_dir(env, config_path)
    }

    pub fn load_from_dir(
        env: &environment::Environment,
        path: impl AsRef<Path>,
    ) -> anyhow::Result<Self> {
        let suffix = env::var(RUNNER_CONFIG_SUFFIX_VARIABLE)
            .unwrap_or_else(|_| DEFAULT_CONFIG_FILE_SUFFIX.to_string());

        let config_file_name = match suffix.is_empty() {
            true => format!("{env}.toml"),
            false => format!("{env}_{suffix}.toml"),
        };

        let config_file = path.as_ref().join(config_file_name);

        let config_builder = config::Config::builder()
            .add_source(config::File::from(config_file).required(false))
            .add_source(config::Environment::with_prefix("kiln").separator("."));

        let config = config_builder.build()?.try_deserialize()?;

        Ok(config)
    }
}
